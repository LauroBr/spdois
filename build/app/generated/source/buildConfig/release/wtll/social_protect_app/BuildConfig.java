/**
 * Automatically generated file. DO NOT MODIFY
 */
package wtll.social_protect_app;

public final class BuildConfig {
  public static final boolean DEBUG = false;
  public static final String APPLICATION_ID = "wtll.social_protect_app";
  public static final String BUILD_TYPE = "release";
  public static final String FLAVOR = "";
  public static final int VERSION_CODE = 5;
  public static final String VERSION_NAME = "1.0.5";
}
