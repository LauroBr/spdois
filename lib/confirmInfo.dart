import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:social_protect_app/systemUser.dart';
import 'globals.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'main.dart';

class ConfirmInfoPage extends StatefulWidget {
  ConfirmInfoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _ConfirmInfoPageState createState() => _ConfirmInfoPageState();
}

final dbRef = FirebaseDatabase.instance.reference();
final _emailController = TextEditingController();
final _personalController = TextEditingController();
bool emailValid = false;

class _ConfirmInfoPageState extends State<ConfirmInfoPage> {
  final _formKey1 = GlobalKey<FormState>();
  @override
  void initState() {
    super.initState();
    _emailController.text = "";
    _personalController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-1.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                  child: Column(
                children: <Widget>[
                  Center(
                      child: Container(
                          margin: EdgeInsets.only(top: 50),
                          width: MediaQuery.of(context).size.width - 120,
                          child: Text(
                            'Confirme as informações abaixo',
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ))),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 310,
                            width: MediaQuery.of(context).size.width - 70,
                            padding: EdgeInsets.all(20),
                            decoration: new BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 15.0, // soften the shadow
                                  spreadRadius: 1.0, //extend the shadow
                                  offset: Offset(
                                    5.0, // Move to right 10  horizontally
                                    5.0, // Move to bottom 10 Vertically
                                  ),
                                )
                              ],
                              color: Colors.white,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(10.0),
                                  topRight: const Radius.circular(10.0),
                                  bottomLeft: const Radius.circular(10.0),
                                  bottomRight: const Radius.circular(10.0)),
                            ),
                            child: SingleChildScrollView(
                                child: Column(
                              children: <Widget>[
                                Form(
                                    key: _formKey1,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('E-mail da Hotmart',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                              textAlign: TextAlign.left),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 5, 0, 15),
                                              child: TextFormField(
                                                controller: _emailController,
                                                decoration: new InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.fromLTRB(
                                                            20, 15, 20, 15),
                                                    filled: true,
                                                    fillColor: Colors.grey[300],
                                                    border:
                                                        new OutlineInputBorder(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                        const Radius.circular(
                                                            25.0),
                                                      ),
                                                    ),
                                                    hintText:
                                                        'E-mail da Hotmart'),
                                                validator: (value) {
                                                  emailValid = RegExp(
                                                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                      .hasMatch(value);
                                                  if (value.isEmpty) {
                                                    return 'Digite seu e-mail';
                                                  }
                                                  return null;
                                                },
                                              )),
                                          Text('CPF/CNPJ',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                              textAlign: TextAlign.left),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 5, 0, 15),
                                              child: TextFormField(
                                                controller: _personalController,
                                                decoration: new InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.fromLTRB(
                                                            20, 15, 20, 15),
                                                    filled: true,
                                                    fillColor: Colors.grey[300],
                                                    border:
                                                        new OutlineInputBorder(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                        const Radius.circular(
                                                            25.0),
                                                      ),
                                                    ),
                                                    hintText: 'CPF/CNPJ'),
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return 'Digite o CPF/CNPJ';
                                                  }
                                                  return null;
                                                },
                                              )),
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  100,
                                              height: 40,
                                              child: RaisedButton(
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                .circular(18.0),
                                                        side: BorderSide(
                                                            color: Colors
                                                                .orangeAccent)),
                                                onPressed: () {
                                                  if (_formKey1.currentState
                                                      .validate()) {
                                                    if (!emailValid) {
                                                      _onAlertButtonPressed(
                                                          context);
                                                    } else {
                                                      _performConfirm();
                                                    }
                                                  }
                                                },
                                                color: Colors.orangeAccent,
                                                textColor: Colors.white,
                                                child: Text("Continuar",
                                                    style: TextStyle(
                                                        fontSize: 18)),
                                              )),
                                        ])),
                              ],
                            )),
                          ),
                        ],
                      )),
                ],
              )))
        ]))));
  }

  void _performConfirm() {
    var email = _emailController.text.replaceAll(' ', '');
    var info = _personalController.text;
    obj.uid = textToMd5(email);
    dbRef
        .reference()
        .child('Users/' + textToMd5(email))
        .once()
        .then((DataSnapshot snapshot) {
      var map = new Map<String, dynamic>.from(snapshot.value);
      var todo = new SystemUser.fromJson(map);
      if ((textToMd5(todo.emailHotmart) == textToMd5(email)) &&
          info == todo.personalInfo) {
        obj.emailHotmart = todo.emailHotmart;
        navigateToNewPasswordPage(context);
      } else {
        _onAlertInfoWrong(context);
      }
    });
  }
}

_onAlertInfoWrong(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Informações inválidas",
    desc: "O e-mail e/ou CPF/CNPJ informados não estão corretos.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onAlertButtonPressed(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está em formato incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}
