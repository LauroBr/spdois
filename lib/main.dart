import 'package:flutter/material.dart';
import 'package:social_protect_app/editInfo.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth1.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth2.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth3.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth4.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth5.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth6.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth7.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth8.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth9.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth10.dart';
import 'package:social_protect_app/notMadeAuth/notMadeAuth11.dart';
import 'package:social_protect_app/madeAuth/madeAuth1.dart';
import 'package:social_protect_app/madeAuth/madeAuth2.dart';
import 'package:social_protect_app/madeAuth/madeAuth3.dart';
import 'package:social_protect_app/madeAuth/madeAuth4.dart';
import 'package:social_protect_app/madeAuth/madeAuth5.dart';
import 'package:social_protect_app/madeAuth/madeAuth6.dart';
import 'package:social_protect_app/madeAuth/madeAuth7.dart';
import 'package:social_protect_app/madeAuth/madeAuth8.dart';
import 'package:social_protect_app/madeAuth/madeAuth9.dart';
import 'package:social_protect_app/madeAuth/madeAuth11.dart';
import 'package:social_protect_app/madeAuth/madeAuth12.dart';
import 'package:social_protect_app/madeAuth/madeAuth13.dart';
import 'package:social_protect_app/verifyInfo/verifyInfo1.dart';
import 'package:social_protect_app/verifyInfo/verifyInfo2.dart';
import 'package:social_protect_app/verifyInfo/verifyInfo3.dart';
import 'package:social_protect_app/verifyInfo/verifyInfo4.dart';
import 'package:url_launcher/url_launcher.dart';
import 'login.dart';
import 'madeAuth/madeAuth10.dart';
import 'selectType.dart';
import 'subscribePJ.dart';
import 'subscribePF.dart';
import 'factors.dart';
import 'fillCode.dart';
import 'fillInfo.dart';
import 'password.dart';
import 'newPassword.dart';
import 'manager.dart';
import 'personalPanel.dart';
import 'signIn.dart';
import 'confirmInfo.dart';
import 'selectInsta.dart';
import 'package:crypto/crypto.dart';
import 'dart:convert';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Social Protect',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: LoginPage(title: 'Social Protect'),
    );
  }
}

 goToSite() async {
    const url = 'http://socialprotect.com.br/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

class Footer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        height: 120,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                width: 220,
                child: GestureDetector(
                  onTap: () {
                    goToSite();
                  },
                  child: Image.asset(
                    'img/Logo-3.png',
                  ),
                )),
            Text("______________________________________",
                style: TextStyle(fontSize: 14, color: Colors.white)),
            Row(
              children: <Widget>[
                InkWell(
                  onTap: () {
                    goTo1();
                  },
                  child: Text("Quem Somos  |",
                      style: TextStyle(fontSize: 18, color: Colors.white)),
                ),
                InkWell(
                  onTap: () {
                    goTo2();
                  },
                  child: Text("  Termos de Serviço",
                      style: TextStyle(fontSize: 18, color: Colors.white)),
                )
              ],
            )
          ],
        ));
  }

  goTo1() async {
    const url = 'http://socialprotect.com.br/quemsomos';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  goTo2() async {
    const url = 'http://socialprotect.com.br/termoseservicos';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[],
        ),
      ),
    );
  }
}

String textToMd5 (String text) {
  return md5.convert(utf8.encode(text)).toString();
}

Future navigateToSubscribePagePF(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribePagePF()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SubscribePage();
  // }));
}

Future navigateToSubscribePagePJ(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribePagePJ()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SubscribePage();
  // }));
}

Future navigateToLoginPage(context) async {
  Navigator.popUntil(context, ModalRoute.withName(Navigator.defaultRouteName));
  //Navigator.pop(context);
  // Navigator.push(context, MaterialPageRoute(builder: (context) => LoginPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return LoginPage();
  // }));
}

Future navigateToFactorsPage(context) async {
  //Navigator.push(context, MaterialPageRoute(builder: (context) => FactorsPage()));
  Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return FactorsPage();
  }));
}

Future navigateToMadeAuth1(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth1()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth1();
  // }));
}

Future navigateToMadeAuth2(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth2()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth2();
  // }));
}

Future navigateToMadeAuth3(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth3()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth3();
  // }));
}

Future navigateToMadeAuth4(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth4()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth4();
  // }));
}

Future navigateToMadeAuth5(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth5()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth5();
  // }));
}

Future navigateToMadeAuth6(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth6()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth6();
  // }));
}

Future navigateToMadeAuth7(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth7()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth7();
  // }));
}

Future navigateToMadeAuth8(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth8()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth8();
  // }));
}

Future navigateToMadeAuth9(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth9()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth9();
  // }));
}

Future navigateToMadeAuth10(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth10()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth10();
  // }));
}

Future navigateToMadeAuth11(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth11()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth11();
  // }));
}

Future navigateToMadeAuth12(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth12()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth12();
  // }));
}

Future navigateToMadeAuth13(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => MadeAuth13()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return MadeAuth13();
  // }));
}

Future navigateToNotMadeAuth1(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth1()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth1();
  // }));
}

Future navigateToNotMadeAuth2(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth2()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth2();
  // }));
}

Future navigateToNotMadeAuth3(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth3()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth3();
  // }));
}

Future navigateToNotMadeAuth4(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth4()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth4();
  // }));
}

Future navigateToNotMadeAuth5(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth5()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth5();
  // }));
}

Future navigateToNotMadeAuth6(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth6()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth6();
  // }));
}

Future navigateToNotMadeAuth7(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth7()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth7();
  // }));
}

Future navigateToNotMadeAuth8(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth8()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth8();
  // }));
}

Future navigateToNotMadeAuth9(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth9()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth9();
  // }));
}

Future navigateToNotMadeAuth10(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth10()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth10();
  // }));
}

Future navigateToNotMadeAuth11(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NotMadeAuth11()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NotMadeAuth11();
  // }));
}

Future navigateToFillCodePage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => FillCodePage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return FillCodePage();
  // }));
}

Future navigateToFillInfoPage(context) async {
  //Navigator.push(context, MaterialPageRoute(builder: (context) => FillInfoPage()));
  Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return FillInfoPage();
  }));
}

Future navigateToFillInfoPage2(context) async {
  Navigator.pop(context);
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return FillInfoPage();
  // }));
}

Future navigateToEditInfoPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => EditInfoPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return EditInfoPage();
  // }));
}

Future navigateToVerifyInfo1(context) async {
  Navigator.push(
      context, MaterialPageRoute(builder: (context) => VerifyInfo1()));
}

Future navigateToVerifyInfo2(context) async {
 Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return VerifyInfo2();
  }));
}

Future navigateToVerifyInfo3(context) async {
  Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return VerifyInfo3();
  }));
}

Future navigateToVerifyInfo4(context) async {
  Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return VerifyInfo4();
  }));
}

Future navigateToPasswordPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => PasswordPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return PasswordPage();
  // }));
}

Future navigateToNewPasswordPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => NewPasswordPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return NewPasswordPage();
  // }));
}

Future navigateToPersonalPanelPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => PersonalPanelPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return PersonalPanelPage();
  // }));
}

Future navigateToPersonalPanelPage2(context) async {
  //Navigator.pop(context);
  Navigator.pushReplacement(context,
      MaterialPageRoute(builder: (BuildContext context) {
    return PersonalPanelPage();
  }));
}

Future navigateToSignInPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => SignInPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SignInPage();
  // }));
}

Future navigateToSelectTypePage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => SelectTypePage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SignInPage();
  // }));
}

Future navigateToSelectInstaPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => SelectInstaPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SignInPage();
  // }));
}

Future navigateToManagerPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => ManagerPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return SignInPage();
  // }));
}

Future navigateToConfirmInfoPage(context) async {
  Navigator.push(context, MaterialPageRoute(builder: (context) => ConfirmInfoPage()));
  // Navigator.pushReplacement(context,
  //     MaterialPageRoute(builder: (BuildContext context) {
  //   return ConfirmInfoPage();
  // }));
}