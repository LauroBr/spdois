import 'package:flutter/material.dart';
import 'package:social_protect_app/main.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:social_protect_app/systemUser.dart';
import 'globals.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:date_format/date_format.dart';
import 'package:url_launcher/url_launcher.dart';
//import 'package:back_button_interceptor/back_button_interceptor.dart';

bool validDate = false;
String finalDate = "";
bool temData = false;
double numChar = 0;
int nInstas = 0;
int validOffers = 0;

class PersonalPanelPage extends StatefulWidget {
  PersonalPanelPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  State<StatefulWidget> createState() {
    return _PersonalPanelPageState();
  }
}

final GlobalKey<NavigatorState> navigatorKey = new GlobalKey<NavigatorState>();
final dbRef = FirebaseDatabase.instance.reference();
List<Widget> list = new List();
List<dynamic> ordersValid = new List();

class _PersonalPanelPageState extends State<PersonalPanelPage> {
  @override
  void initState() {
    super.initState();
    //BackButtonInterceptor.add(myInterceptor);
    user.expirationDate = "";
    if (user.idInstagram.length <= 18) {
      numChar = 22;
    } else if (user.idInstagram.length == 19) {
      numChar = 21;
    } else if (user.idInstagram.length == 20) {
      numChar = 20;
    } else if (user.idInstagram.length == 21) {
      numChar = 19;
    } else if (user.idInstagram.length == 23 || user.idInstagram.length == 22) {
      numChar = 18;
    } else if (user.idInstagram.length == 24) {
      numChar = 17;
    } else if (user.idInstagram.length == 25) {
      numChar = 16;
    } else if (user.idInstagram.length == 26 || user.idInstagram.length == 27) {
      numChar = 15;
    } else if (user.idInstagram.length == 28 || user.idInstagram.length == 29) {
      numChar = 14;
    } else if (user.idInstagram.length == 30) {
      numChar = 13;
    }

    checkDate().then((otherDate) {
      this.setState(() {
        user.expirationDate = finalDate.toString();
      });
    });
  }

  // @override
  // void dispose() {
  //   BackButtonInterceptor.remove(myInterceptor);
  //   super.dispose();
  // }

  // bool myInterceptor(bool stopDefaultButtonEvent) {
  //   navigateToSelectInstaPage(context);
  //   return true;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-2.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                  child: Column(children: <Widget>[
                Row(
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 2,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Container(
                                height: 30,
                                margin: EdgeInsets.fromLTRB(10, 22, 0, 0),
                                child: Row(children: <Widget>[
                                  Container(
                                      margin: EdgeInsets.only(top: 5),
                                      child: Text(
                                        '${user.idInstagram}',
                                        style: TextStyle(
                                            fontSize: numChar,
                                            fontWeight: FontWeight.bold),
                                      )),
                                  Container(
                                      width: 24,
                                      child: IconButton(
                                          icon: new Icon(
                                            Icons.create,
                                            color: Colors.blue,
                                            size: 22.0,
                                          ),
                                          onPressed: () {
                                            navigateToEditInfoPage(context);
                                          }))
                                ])),
                            _buildDate()
                          ],
                        )),
                    Container(
                      padding: EdgeInsets.fromLTRB(0, 5, 5, 0),
                      width: MediaQuery.of(context).size.width / 2,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: <Widget>[
                          Container(child: _buildIcon()),
                          Container(
                              child: Row(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[_buildExpanded()],
                          ))
                        ],
                      ),
                    )
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      margin: EdgeInsets.only(top: 50),
                      child: Column(children: <Widget>[
                        _buildButton(),
                        Container(
                            margin: EdgeInsets.only(top: 25),
                            width: 245,
                            height: 50,
                            child: RaisedButton(
                              onPressed: () {
                                goToFAQ();
                              },
                              color: Colors.grey[200],
                              textColor: Colors.white,
                              child: Text("> Enviar uma solicitação",
                                  style: TextStyle(
                                      fontSize: 16,
                                      fontWeight: FontWeight.bold,
                                      color: Colors.blue[600])),
                            )),
                      ]),
                    )
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                ),
                Center(
                    child:
                        Row(mainAxisSize: MainAxisSize.max, children: <Widget>[
                  Flexible(
                    child: Container(
                        padding: EdgeInsets.symmetric(vertical: 3),
                        margin: EdgeInsets.symmetric(horizontal: 50),
                        child: Text(
                          "Receba alertas de Segurança direto no seu Smartphone",
                          style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.bold,
                              color: Colors.black),
                          textAlign: TextAlign.center,
                        )),
                  )
                ])),
                Padding(
                  padding: EdgeInsets.only(top: 40),
                ),
                Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Container(
                            width: 120,
                            child: GestureDetector(
                              onTap: () {
                                goToInstagram();
                              },
                              child: Image.asset(
                                'img/Telegram-Btn.png',
                              ),
                            )),
                        Padding(
                          padding: EdgeInsets.only(left: 15),
                        ),
                        Container(
                            child: Text(
                          'ou',
                          style: TextStyle(
                              color: Colors.white,
                              fontWeight: FontWeight.bold,
                              fontSize: 20),
                        )),
                        Padding(
                          padding: EdgeInsets.only(left: 15),
                        ),
                        Container(
                          width: 120,
                          child: GestureDetector(
                              onTap: () {
                                goToWPP();
                              },
                              child: Image.asset(
                                'img/WPP-Btn.png',
                              )),
                        )
                      ],
                    )
                  ],
                ),
              ])))
        ]))));
  }

  goToFAQ() async {
    const url =
        'https://wa.me/5511972544400?text=Ol%C3%A1!%20Desejo%20realizar%20uma%20solicita%C3%A7%C3%A3o';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  goToInstagram() async {
    const url = 'https://t.me/joinchat/AAAAAFSD5Na-6kpbDdb8PQ';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  goToWPP() async {
    const url =
        'https://api.whatsapp.com/send?phone=5522997902108&text=Gostaria%20de%20receber%20os%20alertas%20de%20seguran%c3%a7a%20no%20meu%20WhatsApp&source=&data=';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

Future checkDate() async {
  var response = await http.post(Uri.encodeFull(url), headers: {
    "Authorization":
        "Basic ODFkMDQzMGQtNGJlMy00MzBjLTljNTYtZWM2ZWIwNTBmZjZmOjA5MjQyNGMxLTU2ZmItNDk1Yi04YjRiLWMzZWZiNGE5YzFhYg=="
  });
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    var getHistory = await http.get(Uri.encodeFull(urlHistory),
        headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
    var history = convert.jsonDecode(getHistory.body);
    var getOffers = await http.get(Uri.encodeFull(urlOffers),
        headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
    offers = convert.jsonDecode(getOffers.body);
    dbRef
        .reference()
        .child('Users/' + textToMd5(obj.emailHotmart))
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot != null) {
        var map = new Map<String, dynamic>.from(snapshot.value);
        SystemUser usuario = new SystemUser.fromJson(map);
        if (usuario.instas != null) {
          List<dynamic> instas = usuario.instas.values.toList();
          nInstas = instas.length;
        }
      }
    });
    validDate = false;
    ordersValid.clear();
    validOffers = 0;
    for (int i = 0; i < history["data"].length; i++) {
      if (history["data"][i]["buyer"]["email"] == user.emailHotmart) {
        // &&history["data"][i]["purchase"]["transaction"] == user.codeHotmart
        verifyDate(
            history["data"][i],
            history["data"][i]["purchase"]["approvedDate"],
            history["data"][i]["offer"]["key"]);
      }
    }
    if (containsCurrentOrder(user.codeHotmart)) {
      validDate = true;
      temData = true;
    } else {
      if (validOffers >= nInstas && validOffers > 0) {
        validDate = true;
        temData = false;
      } else {
        validDate = false;
        temData = false;
      }
    }
  } else {
    //mensagem de deslogar
  }
}

verifyDate(order, approvalDate, key) {
  var date = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(approvalDate.toString()))
      .toLocal();
  finalDate = date.toString();
  String signature = "";
  for (int i = 0; i < offers["data"].length; i++) {
    if (offers["data"][i]["key"] == key) {
      signature = offers["data"][i]["planPeriodicity"];
    }
  }

  if (signature == "QUARTERLY") {
    date = date.add(new Duration(days: 90));
  } else if (signature == "BIANNUAL") {
    date = date.add(new Duration(days: 180));
  } else if (signature == "ANNUAL") {
    date = date.add(new Duration(days: 365));
  } else if (signature == "MONTHLY") {
    date = date.add(new Duration(days: 31));
  }
  if (date.isAfter(DateTime.now())) {
    validOffers++;
    ordersValid.add(order);
  }
  int year = date.year;
  int month = date.month;
  int day = date.day;
  finalDate = formatDate(DateTime(year, month, day), [dd, '/', mm, '/', yyyy]);
}

bool containsCurrentOrder(codeHotmart) {
  List<dynamic> list = ordersValid.toList();
  for (int i = 0; i < list.length; i++) {
    if (list[i]["purchase"]["transaction"] == codeHotmart) {
      calcDate(list[i], list[i]["purchase"]["approvedDate"],
          list[i]["offer"]["key"], offers);
      return true;
    }
  }
  return false;
}

void calcDate(order, approvalDate, key, offers) {
  var date = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(approvalDate.toString()))
      .toLocal();
  finalDate = date.toString();
  String signature = "";
  for (int i = 0; i < offers["data"].length; i++) {
    if (offers["data"][i]["key"] == key) {
      signature = offers["data"][i]["planPeriodicity"];
    }
  }

  if (signature == "QUARTERLY") {
    date = date.add(new Duration(days: 90));
  } else if (signature == "BIANNUAL") {
    date = date.add(new Duration(days: 180));
  } else if (signature == "ANNUAL") {
    date = date.add(new Duration(days: 365));
  } else if (signature == "MONTHLY") {
    date = date.add(new Duration(days: 31));
  }

  int year = date.year;
  int month = date.month;
  int day = date.day;
  finalDate = formatDate(DateTime(year, month, day), [dd, '/', mm, '/', yyyy]);
}

_buildIcon() {
  if (validDate) {
    return InstagramIconNotExpired();
  } else {
    return InstagramIconExpired();
  }
}

_buildExpanded() {
  if (validDate) {
    return ExpandedNotExpired();
  } else {
    return ExpandedExpired();
  }
}

_buildDate() {
  if (validDate) {
    if (temData) {
      return InstagramDateNotExpired();
    } else {
      return InstagramDateNotExpired2();
    }
  } else {
    return InstagramDateExpired();
  }
}

_buildButton() {
  if (validDate) {
    if (user.type == "Conta Empresa")
      return ButtonNotExpiredPJ();
    else {
      return ButtonNotExpiredPF();
    }
  } else {
    if (user.type == "Conta Empresa")
      return ButtonExpiredPJ();
    else {
      return ButtonExpiredPF();
    }
  }
}

class ButtonExpiredPJ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 50,
        child: RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.orangeAccent)),
            onPressed: () {
              navigateToSubscribePagePJ(context);
            },
            color: Colors.orangeAccent,
            textColor: Colors.white,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text("Renovar Assinatura",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            )));
  }
}

class ButtonExpiredPF extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 50,
        child: RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.orangeAccent)),
            onPressed: () {
              navigateToSubscribePagePF(context);
            },
            color: Colors.orangeAccent,
            textColor: Colors.white,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text("Renovar Assinatura",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            )));
  }
}

class ButtonNotExpiredPJ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 50,
        child: RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.blue[600])),
            onPressed: () {
              navigateToSubscribePagePJ(context);
            },
            color: Colors.blue[600],
            textColor: Colors.white,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text("Estender Assinatura",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            )));
  }
}

class ButtonNotExpiredPF extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 50,
        child: RaisedButton(
            shape: new RoundedRectangleBorder(
                borderRadius: new BorderRadius.circular(25.0),
                side: BorderSide(color: Colors.blue[600])),
            onPressed: () {
              navigateToSubscribePagePF(context);
            },
            color: Colors.blue[600],
            textColor: Colors.white,
            child: FittedBox(
              fit: BoxFit.fitWidth,
              child: Text("Estender Assinatura",
                  style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold)),
            )));
  }
}

class InstagramDateExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 40,
        padding: EdgeInsets.fromLTRB(10, 0, 0, 3),
        child: Text(
          'Seu Instagram está        desprotegido',
          style: TextStyle(fontSize: 16),
        ));
  }
}

class InstagramDateNotExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 40,
        padding: EdgeInsets.fromLTRB(10, 0, 0, 3),
        child: Text(
          'Seu Instagram está seguro até ${user.expirationDate}',
          style: TextStyle(fontSize: 16),
        ));
  }
}

class InstagramDateNotExpired2 extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        width: 250,
        height: 40,
        padding: EdgeInsets.fromLTRB(10, 0, 0, 3),
        child: Text(
          'Seu Instagram está seguro',
          style: TextStyle(fontSize: 16),
        ));
  }
}

class InstagramIconExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Icon(
        Icons.cancel,
        color: Colors.red,
        size: 30.0,
      ),
    );
  }
}

class InstagramIconNotExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Icon(
        Icons.check_circle,
        color: Colors.green,
        size: 30.0,
      ),
    );
  }
}

class ExpandedExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InstagramMessageExpired();
  }
}

class InstagramMessageExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.lock_open,
          color: Colors.blue[600],
          size: 20.0,
        ),
        Text(
          ' Inseguro',
          style: TextStyle(
            fontSize: 14,
            color: Colors.blue[600],
          ),
        )
      ],
    ));
  }
}

class ExpandedNotExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InstagramMessageNotExpired();
  }
}

class InstagramMessageNotExpired extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Row(
      mainAxisAlignment: MainAxisAlignment.end,
      children: <Widget>[
        Icon(
          Icons.lock,
          color: Colors.blue[600],
          size: 20.0,
        ),
        Text(
          ' Protegido',
          style: TextStyle(fontSize: 14, color: Colors.blue[600]),
        )
      ],
    ));
  }
}
