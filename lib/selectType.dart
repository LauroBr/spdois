import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:social_protect_app/globals.dart';
import 'package:url_launcher/url_launcher.dart';
import 'main.dart';

class SelectTypePage extends StatefulWidget {
  SelectTypePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SelectTypePageState createState() => _SelectTypePageState();
}

final dbRef = FirebaseDatabase.instance.reference();
class _SelectTypePageState extends State<SelectTypePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
          child: SingleChildScrollView(
            child: Column(
              children: <Widget>[
                Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-1.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                child: Column(children: <Widget>[
                Container(
                    margin: EdgeInsets.only(top: 30),
                    padding: EdgeInsets.symmetric(horizontal: 100, vertical: 20),
                    child: Text(
                      'Escolha qual plano você deseja cadastrar',
                      style: TextStyle(
                          fontSize: 20,
                          fontWeight: FontWeight.bold,
                          color: Colors.white),
                      textAlign: TextAlign.center,
                    )),
                Container(
                    child: Column(children: <Widget>[
                      Padding(padding: EdgeInsets.only(top:20),),
                      Container(
                        height: 180,
                        width: 200,
                        padding: EdgeInsets.all(20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 15.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                5.0, // Move to right 10  horizontally
                                5.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top:20),),
                            Container(
                            width: 220,
                            child: GestureDetector(
                              onTap: () {
                                _performChoosePF();
                              },
                              child: Image.asset(
                                'img/solo.png',
                                width: 80,
                                height: 75,
                              ),
                            )),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 30,
                              width: 130,
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(18.0),
                                    side: BorderSide(color: Colors.orangeAccent)),
                                onPressed: () {_performChoosePF();},
                                color: Colors.orangeAccent,
                                textColor: Colors.white,
                                child: Text("Conta pessoal", style: TextStyle(fontSize: 12)),
                              ))
                          ],
                        )),
                      ),
                      Padding(padding: EdgeInsets.only(top:40),),
                      Container(
                        height: 180,
                        width: 200,
                        padding: EdgeInsets.all(20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 15.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                5.0, // Move to right 10  horizontally
                                5.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(
                          children: <Widget>[
                            Padding(padding: EdgeInsets.only(top:20),),
                            Container(
                            width: 220,
                            child: GestureDetector(
                              onTap: () {
                                _performChoosePJ();
                              },
                              child: Image.asset(
                                'img/group.png',
                                width: 80,
                                height: 75,
                              ),
                            )),
                            Container(
                              margin: EdgeInsets.only(top: 15),
                              height: 30,
                              width: 130,
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(
                                    borderRadius: new BorderRadius.circular(18.0),
                                    side: BorderSide(color: Colors.orangeAccent)),
                                onPressed: () {_performChoosePJ();},
                                color: Colors.orangeAccent,
                                textColor: Colors.white,
                                child: Text("Conta empresa", style: TextStyle(fontSize: 12)),
                              ))  
                          ],
                        )),
                      ),
                      Padding(padding: EdgeInsets.only(top:30),),
                      Container(
                        width: 220,
                        child: GestureDetector(
                          onTap: () {
                            goToSite();
                          },
                          child: Image.asset(
                            'img/Logo-3.png',
                          ),
                        )),
                ]))
              ])))
        ]))
        )
        );
  }

  goToSite() async {
    const url = 'http://socialprotect.com.br/';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
  void _performChoosePF() {
    user.type = "Conta Pessoal";
    navigateToFillInfoPage(context);
  }
  
  void _performChoosePJ() {
    user.type = "Conta Empresa";
    navigateToFillInfoPage(context);
  }
}


  