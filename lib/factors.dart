import 'package:back_button_interceptor/back_button_interceptor.dart';
import 'package:flutter/material.dart';
import 'main.dart';

class FactorsPage extends StatefulWidget {
  FactorsPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _FactorsPageState createState() => _FactorsPageState();
}

class _FactorsPageState extends State<FactorsPage> {
  @override
  void initState() {
    super.initState();
    BackButtonInterceptor.add(myInterceptor);
  }

  @override
  void dispose() {
    BackButtonInterceptor.remove(myInterceptor);
    super.dispose();
  }

  bool myInterceptor(bool stopDefaultButtonEvent) {
    navigateToSelectInstaPage(context);
    return true;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Container(
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
      Container(
          padding: EdgeInsets.fromLTRB(20, 10, 20, 15),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/BG-1.png"),
              fit: BoxFit.fill,
            ),
          ),
          child: Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top: 30),
              ),
              Row(
                children: <Widget>[
                  Container(
                      padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      height: 230,
                      width: MediaQuery.of(context).size.width - 40,
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.white30,
                            spreadRadius: 2.0, //extend the shadow
                            offset: Offset(
                              1.0, // Move to right 10  horizontally
                              5.0, // Move to bottom 10 Vertically
                            ),
                          )
                        ],
                        color: Colors.grey[100],
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: Column(
                        children: <Widget>[
                          Container(
                              padding: EdgeInsets.fromLTRB(15, 30, 15, 10),
                              child: Text(
                                'Você precisa autentificar sua conta antes de continuar!',
                                style: TextStyle(
                                    color: Colors.blueAccent[700],
                                    fontSize: 28,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              )),
                          Container(
                              padding: EdgeInsets.fromLTRB(30, 0, 30, 0),
                              child: Text(
                                'escolha uma opção abaixo',
                                style: TextStyle(
                                    color: Colors.grey,
                                    fontSize: 20,
                                    fontWeight: FontWeight.bold),
                                textAlign: TextAlign.center,
                              ))
                        ],
                      ))
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Container(
                  width: MediaQuery.of(context).size.width - 100,
                  height: 60,
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                        side: BorderSide(color: Colors.orangeAccent)),
                    onPressed: () {
                      navigateToMadeAuth1(context);
                    },
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    color: Colors.orangeAccent,
                    textColor: Colors.white,
                    child: Text("Já fiz a autenticação de dois fatores.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  )),
              Padding(
                padding: EdgeInsets.only(top: 15),
              ),
              Container(
                  width: MediaQuery.of(context).size.width - 100,
                  height: 60,
                  child: RaisedButton(
                    shape: new RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(30.0),
                        side: BorderSide(color: Colors.orangeAccent)),
                    onPressed: () {
                      navigateToNotMadeAuth1(context);
                    },
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    color: Colors.orangeAccent,
                    textColor: Colors.white,
                    child: Text("Não tenho a autenticação de dois fatores.",
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  )),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Column(
                    children: <Widget>[Footer()],
                  )
                ],
              )
            ],
          )),
    ]))));
  }
}
