import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:social_protect_app/globals.dart' as prefix1;
import 'package:social_protect_app/systemUser.dart';
import 'main.dart';
import 'globals.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;

class EditInfoPage extends StatefulWidget {
  EditInfoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _EditInfoPageState createState() => _EditInfoPageState();
}

final dbRef = FirebaseDatabase.instance.reference();
String codigoUsado;
bool emailValid = false;
bool instaFilled = false;
bool emailFilled = false;
bool telFilled = false;
bool codeFilled = false;
bool alterou = false;
final _instaController = TextEditingController();
final _emailController = TextEditingController();
final _telController = TextEditingController();
final _codeController = TextEditingController();
List<dynamic> ordersValid = new List();

class _EditInfoPageState extends State<EditInfoPage> {
  final _formKey20 = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _codeController.text = prefix1.user.codeHotmart;
    _emailController.text = prefix1.user.emailInstagram;
    _instaController.text = prefix1.user.idInstagram;
    _telController.text = prefix1.user.telInstagram;
    codigoUsado = _codeController.text;
    verifyNumber();
  }

  @override
  Widget build(BuildContext context) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("img/BG-1.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: SingleChildScrollView(
                    child: Padding(
                        padding: EdgeInsets.only(bottom: bottom),
                        child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 40),
                          ),
                          Container(
                            child: Column(children: <Widget>[
                              Container(
                                  height: 380,
                                  width: MediaQuery.of(context).size.width - 70,
                                  padding: EdgeInsets.fromLTRB(20, 15, 20, 0),
                                  decoration: new BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 15.0, // soften the shadow
                                        spreadRadius: 1.0, //extend the shadow
                                        offset: Offset(
                                          5.0, // Move to right 10  horizontally
                                          5.0, // Move to bottom 10 Vertically
                                        ),
                                      )
                                    ],
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0),
                                        bottomLeft: const Radius.circular(10.0),
                                        bottomRight:
                                            const Radius.circular(10.0)),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      // Column(
                                      //     crossAxisAlignment:
                                      //         CrossAxisAlignment.end,
                                      //     children: <Widget>[
                                      //       Container(),
                                      //       Container(
                                      //           height: 25,
                                      //           child: IconButton(
                                      //               icon: new Icon(
                                      //                 Icons.remove,
                                      //                 color: Colors.red,
                                      //                 size: 30.0,
                                      //               ),
                                      //               onPressed: () {
                                      //                 _onExcludeInstagram(
                                      //                     context);
                                      //               }))
                                      //     ]),
                                      Form(
                                          key: _formKey20,
                                          child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              children: <Widget>[
                                                Text('Instagram',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                    textAlign: TextAlign.left),
                                                Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 5, 0, 15),
                                                    child: TextFormField(
                                                      controller:
                                                          _instaController,
                                                      enabled: false,
                                                      decoration:
                                                          new InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          20,
                                                                          15,
                                                                          20,
                                                                          15),
                                                              filled: true,
                                                              fillColor: Colors
                                                                  .grey[400],
                                                              border:
                                                                  new OutlineInputBorder(
                                                                borderRadius:
                                                                    const BorderRadius
                                                                        .all(
                                                                  const Radius
                                                                          .circular(
                                                                      25.0),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  'Id do Instagram'),
                                                      validator: (value) {
                                                        if (value.isEmpty) {
                                                          instaFilled = false;
                                                        } else {
                                                          instaFilled = true;
                                                        }
                                                        return null;
                                                      },
                                                    )),
                                                Text('E-mail do Instagram',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                    textAlign: TextAlign.left),
                                                Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 5, 0, 15),
                                                    child: TextFormField(
                                                      controller:
                                                          _emailController,
                                                      decoration:
                                                          new InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          20,
                                                                          15,
                                                                          20,
                                                                          15),
                                                              filled: true,
                                                              fillColor: Colors
                                                                  .grey[300],
                                                              border:
                                                                  new OutlineInputBorder(
                                                                borderRadius:
                                                                    const BorderRadius
                                                                        .all(
                                                                  const Radius
                                                                          .circular(
                                                                      25.0),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  'user@socialprotect.com'),
                                                      validator: (value) {
                                                        emailValid = RegExp(
                                                                r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                            .hasMatch(value);
                                                        if (value.isEmpty) {
                                                          emailFilled = false;
                                                        } else {
                                                          emailFilled = true;
                                                        }
                                                        return null;
                                                      },
                                                    )),
                                                Text('Telefone do Instagram',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                    textAlign: TextAlign.left),
                                                Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 5, 0, 15),
                                                    child: TextFormField(
                                                      controller:
                                                          _telController,
                                                      keyboardType:
                                                          TextInputType.number,
                                                      decoration:
                                                          new InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          20,
                                                                          15,
                                                                          20,
                                                                          15),
                                                              filled: true,
                                                              fillColor: Colors
                                                                  .grey[300],
                                                              border:
                                                                  new OutlineInputBorder(
                                                                borderRadius:
                                                                    const BorderRadius
                                                                        .all(
                                                                  const Radius
                                                                          .circular(
                                                                      25.0),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  'xx-xxxxx-xxxx'),
                                                      validator: (value) {
                                                        if (value.isEmpty) {
                                                          telFilled = false;
                                                        } else {
                                                          telFilled = true;
                                                        }
                                                        return null;
                                                      },
                                                    )),
                                                Text('Código da Hotmart',
                                                    style: TextStyle(
                                                        fontWeight:
                                                            FontWeight.bold,
                                                        fontSize: 16),
                                                    textAlign: TextAlign.left),
                                                Padding(
                                                    padding:
                                                        EdgeInsets.fromLTRB(
                                                            0, 5, 0, 15),
                                                    child: TextFormField(
                                                      controller:
                                                          _codeController,
                                                      decoration:
                                                          new InputDecoration(
                                                              contentPadding:
                                                                  EdgeInsets
                                                                      .fromLTRB(
                                                                          20,
                                                                          15,
                                                                          20,
                                                                          15),
                                                              filled: true,
                                                              fillColor: Colors
                                                                  .grey[300],
                                                              border:
                                                                  new OutlineInputBorder(
                                                                borderRadius:
                                                                    const BorderRadius
                                                                        .all(
                                                                  const Radius
                                                                          .circular(
                                                                      25.0),
                                                                ),
                                                              ),
                                                              hintText:
                                                                  'Código da Hotmart'),
                                                      validator: (value) {
                                                        if (value.isEmpty) {
                                                          codeFilled = false;
                                                        } else {
                                                          codeFilled = true;
                                                        }
                                                        return null;
                                                      },
                                                    )),
                                              ])),
                                    ],
                                  ))
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 20),
                            height: 130,
                            width: 270,
                            padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                            decoration: new BoxDecoration(
                              border: Border.all(color: Colors.white),
                              color: Colors.transparent,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(5.0),
                                  topRight: const Radius.circular(5.0),
                                  bottomLeft: const Radius.circular(5.0),
                                  bottomRight: const Radius.circular(5.0)),
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'O e-mail e telefone são os mesmos utilizados em seu Instagram?',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  '_____________________________',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width: 110,
                                          height: 28,
                                          child: RaisedButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 0),
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        18.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),
                                            onPressed: () {
                                              if (_formKey20.currentState
                                                  .validate()) {
                                                if (!instaFilled) {
                                                  _onInstaNotFilled(context);
                                                } else if (!emailFilled) {
                                                  _onEmailNotFilled(context);
                                                } else if (!emailValid) {
                                                  _onEmailIncorrect(context);
                                                } else if (!telFilled) {
                                                  _onTelNotFilled(context);
                                                } else if (!codeFilled) {
                                                  _onCodeNotFilled(context);
                                                } else {
                                                  _performInfo();
                                                }
                                              }
                                            },
                                            color: Colors.grey[100],
                                            textColor: Colors.blue,
                                            child: Text("Sim, salvar",
                                                style: TextStyle(fontSize: 16)),
                                          )),
                                      Container(
                                          margin: EdgeInsets.only(left: 10),
                                          width: 110,
                                          height: 28,
                                          child: RaisedButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 0),
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        18.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),
                                            onPressed: () {
                                              navigateToPersonalPanelPage2(
                                                  context);
                                            },
                                            color: Colors.grey[100],
                                            textColor: Colors.blue,
                                            child: Text("Voltar",
                                                style: TextStyle(fontSize: 16)),
                                          )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 30),
                          )
                        ])))))));
  }

  void _performInfo() {
    if (codigoUsado != _codeController.text) {
      alterou = true;
    } else {
      alterou = false;
    }
  
    prefix1.user.codeHotmart = _codeController.text;
    prefix1.user.idInstagram = _instaController.text;
    prefix1.user.emailInstagram = _emailController.text;
    prefix1.user.telInstagram = _telController.text;
    prefix1.user.emailHotmart = prefix1.obj.emailHotmart;
    prefix1.user.expirationDate = "";

    String key;
    bool taUsando = false;

    dbRef
        .reference()
        .child('Users/' + textToMd5(prefix1.obj.emailHotmart))
        .once()
        .then((DataSnapshot snapshot) {
      if (snapshot != null) {
        var map = new Map<String, dynamic>.from(snapshot.value);
        SystemUser usuario = new SystemUser.fromJson(map);
        if (usuario.instas != null) {
          List<dynamic> instas = usuario.instas.values.toList();
          List<dynamic> keys = usuario.instas.keys.toList();
          for (int i = 0; i < usuario.instas.length; i++) {
            if (instas[i]["idInstagram"] == prefix1.user.idInstagram) {
              key = keys[i];
            }
             if (instas[i]["codeHotmart"] == prefix1.user.codeHotmart && instas[i]["idInstagram"] != prefix1.user.idInstagram) {
               taUsando = true;
             }
          }
          if (!alterou) {
            dbRef
                .child("Users/" + textToMd5(prefix1.obj.emailHotmart))
                .child('instas/')
                .child(key)
                .set(prefix1.user.toJson());
            navigateToPersonalPanelPage2(context);
          }
          else {
            if (taUsando) { //vejo se está em uso
              _onHPExists(context);
            }
            else if (!containsCurrentOrder(_codeController.text)){//vejo se está valido
              _onHPExpired(context);
            } 
            else{
              dbRef
                  .child("Users/" + textToMd5(prefix1.obj.emailHotmart))
                  .child('instas/')
                  .child(key)
                  .set(prefix1.user.toJson());
              navigateToPersonalPanelPage2(context);
            }      
          }
        }
      }
    });
  }
}

Future verifyNumber() async {
  var response = await http.post(Uri.encodeFull(url), headers: {
    "Authorization":
        "Basic ODFkMDQzMGQtNGJlMy00MzBjLTljNTYtZWM2ZWIwNTBmZjZmOjA5MjQyNGMxLTU2ZmItNDk1Yi04YjRiLWMzZWZiNGE5YzFhYg=="
  });
  var jsonResponse = convert.jsonDecode(response.body);
  var getHistory = await http.get(Uri.encodeFull(urlHistory),
      headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
  var history = convert.jsonDecode(getHistory.body);

  var getOffers = await http.get(Uri.encodeFull(urlOffers),
      headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
  offers = convert.jsonDecode(getOffers.body);

  for (int i = 0; i < history["data"].length; i++) {
    if (history["data"][i]["buyer"]["email"] == obj.emailHotmart) {
      verifyDate(
          history["data"][i],
          history["data"][i]["purchase"]["approvedDate"],
          history["data"][i]["offer"]["key"]);
    }
  }
}

verifyDate(order, approvalDate, key) {
  var date = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(approvalDate.toString()))
      .toLocal();
  String signature = "";
  for (int i = 0; i < offers["data"].length; i++) {
    if (offers["data"][i]["key"] == key) {
      signature = offers["data"][i]["planPeriodicity"];
    }
  }

  if (signature == "QUARTERLY") {
    date = date.add(new Duration(days: 90));
  } else if (signature == "BIANNUAL") {
    date = date.add(new Duration(days: 180));
  } else if (signature == "ANNUAL") {
    date = date.add(new Duration(days: 365));
  } else if (signature == "MONTHLY") {
    date = date.add(new Duration(days: 31));
  }
  if (date.isAfter(DateTime.now())) {
    ordersValid.add(order);
  }
}

bool containsCurrentOrder(codeHotmart) {
  List<dynamic> list = ordersValid.toList();
  for (int i = 0; i < list.length; i++) {
    if (list[i]["purchase"]["transaction"] == codeHotmart) {
      return true;
    }
  }
  return false;
}

_onHPExpired(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código Hotmart inválido!",
    desc: "O código Hotmart informado está inválido.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

// Future loadInfo() async {
//   dbRef
//       .reference()
//       .child('Users/' + textToMd5(prefix1.obj.emailHotmart))
//       .once()
//       .then((DataSnapshot snapshot) {
//     var map = new Map<String, dynamic>.from(snapshot.value);
//     var obj = new SystemUser.fromJson(map);
//   });
// }

// _onExcludeInstagram(context) {
//   Alert(
//     context: context,
//     type: AlertType.warning,
//     title: "Excluir Instagram",
//     desc:
//         "A exclusão não poderá ser desfeita. Tem certeza que deseja continuar?",
//     buttons: [
//       DialogButton(
//         child: Text(
//           "Sim, entendi",
//           style: TextStyle(color: Colors.white, fontSize: 18),
//         ),
//         onPressed: () {
//           String key;
//           dbRef
//               .reference()
//               .child('Users/' + textToMd5(prefix1.obj.emailHotmart))
//               .once()
//               .then((DataSnapshot snapshot) {
//             if (snapshot != null) {
//               var map = new Map<String, dynamic>.from(snapshot.value);
//               SystemUser usuario = new SystemUser.fromJson(map);
//               if (usuario.instas.values != null) {
//                 List<dynamic> instas = usuario.instas.values.toList();
//                 List<dynamic> keys = usuario.instas.keys.toList();
//                 for (int i = 0; i < usuario.instas.length; i++) {
//                   if (instas[i]["codeHotmart"] == prefix1.user.codeHotmart) {
//                     key = keys[i];
//                   }
//                 }
//                 dbRef
//                     .child("Users/" + textToMd5(prefix1.obj.emailHotmart))
//                     .child('instas/')
//                     .child(key)
//                     .remove();
//                 navigateToLoginPage(context);
//               }
//             }
//           });
//         },
//         width: 120,
//       )
//     ],
//   ).show();
// }

// _onInstaExists(context) {
//   Alert(
//     context: context,
//     type: AlertType.error,
//     title: "Instagram já cadastrado!",
//     desc: "O Id de Instagram informado já foi cadastrado nesta conta.",
//     buttons: [
//       DialogButton(
//         child: Text(
//           "Entendi",
//           style: TextStyle(color: Colors.white, fontSize: 20),
//         ),
//         onPressed: () => Navigator.pop(context),
//         width: 120,
//       ),
//     ],
//   ).show();
// }

_onHPExists(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código Hotmart já cadastrado!",
    desc: "O código Hotmart informado já foi cadastrado anteriormente.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onEmailIncorrect(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está em formato incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onInstaNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "ID do Instagram em branco",
    desc: "Preencha o ID do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onEmailNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail do Instagram em branco",
    desc: "Preencha o e-mail do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onTelNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Telefone do Instagram em branco",
    desc: "Preencha o telefone do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onCodeNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código da Hotmart em branco",
    desc: "Preencha o código da Hotmart para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}
