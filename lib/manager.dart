import 'package:flutter/material.dart';
import 'package:social_protect_app/userData.dart';
import 'package:firebase_database/firebase_database.dart';

class ManagerPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ManagerPageState(this);

  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Column(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("img/BG-1.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Column(children: <Widget>[
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 20),
                  ),
                  Container(
                      child: Column(
                    children: <Widget>[
                      Container(
                        height: MediaQuery.of(context).size.height - 60,
                        width: 350,
                        padding: EdgeInsets.all(10),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 15.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                5.0, // Move to right 10  horizontally
                                5.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: Column(children: <Widget>[
                          SizedBox(
                              height: 540, // fixed height
                              child: ListView(
                                scrollDirection: Axis.vertical,
                                shrinkWrap: true,
                                padding: const EdgeInsets.all(8),
                                children: <Widget>[
                                  Container(
                                    child: Column(children: <Widget>[
                                      for (UserData i in lista)
                                        Row(
                                          children: <Widget>[
                                            Expanded(
                                                flex: 2,
                                                child: Text(
                                                  i.idInstagram.toString(),
                                                  textAlign: TextAlign.left,
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                )),
                                            Padding(
                                              padding: EdgeInsets.only(top: 45),
                                            ),
                                            Expanded(
                                                flex: 4,
                                                child: Text("teste",
                                                  textAlign: TextAlign.left,
                                                  style:
                                                      TextStyle(fontSize: 16),
                                                )),
                                          ],
                                        ),
                                    ]),
                                  ),
                                ],
                              )),
                          Padding(
                            padding: EdgeInsets.symmetric(vertical: 5),
                          ),
                          Container(
                              alignment: Alignment.bottomCenter,
                              width: MediaQuery.of(context).size.width,
                              height: 40,
                              child: RaisedButton(
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(18.0),
                                    side:
                                        BorderSide(color: Colors.orangeAccent)),
                                onPressed: () {
                                  _performBack(context);
                                },
                                color: Colors.orangeAccent,
                                textColor: Colors.white,
                                child: Text("Avançar",
                                    style: TextStyle(fontSize: 18)),
                              )),
                        ]),
                      )
                    ],
                  )),
                ]))
          ],
        )));
  }

  void _performBack(context) {
    Navigator.pop(context);
  }

  void onLoad(BuildContext context) {
    loadUsers();
  }

  void loadUsers() {
    print('entrou');
    lista.clear();
    dbRef.reference().child('Insta/').once().then((DataSnapshot snapshot) {
      Map<dynamic, dynamic> map = snapshot.value;
      map.forEach((key, value) {
        UserData uss = UserData.empty();
        uss.idInstagram = value['idInstagram'];
        uss.emailHotmart = value['emailHotmart'];
        // uss.factorsCode = value['factorsCode'];
        // uss.factorsCode = adicionarCaracteresEspeciais(value['factorsCode']);
        lista.add(uss);
      });
    });
  }

  String adicionarCaracteresEspeciais(texto) {
    String novoTexto = '';
    for (var i = 0; i < texto.length; i++) {
      if (i >= 4) {
        if (i % 4 == 0 && i < texto.length) {
          if (i % 8 == 0 && i < texto.length) novoTexto += ",";
          novoTexto += " ";
        }
      }
      novoTexto += texto[i];
    }
    return novoTexto;
  }
}

final dbRef = FirebaseDatabase.instance.reference();
List<UserData> lista = new List<UserData>();

class _ManagerPageState extends State<ManagerPage> {
  ManagerPage widget;

  _ManagerPageState(this.widget);

  @override
  Widget build(BuildContext context) => widget.build(context);

  @override
  void initState() {
    super.initState();
    widget.onLoad(context);
  }
}
