import 'package:flutter/material.dart';
import 'package:social_protect_app/globals.dart';
import 'main.dart';
import 'systemUser.dart';
import 'userData.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'package:cpfcnpj/cpfcnpj.dart';

class SignInPage extends StatefulWidget {
  SignInPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SignInPageState createState() => _SignInPageState();
}

final dbRef = FirebaseDatabase.instance.reference();

SystemUser obj = new SystemUser.empty();
UserData user = new UserData.empty();
bool emailValid = false;
bool cpfValid;
bool emailExists = true;
final _emailController = TextEditingController();
final _personalController = TextEditingController();
final _formKey7 = GlobalKey<FormState>();
bool podeCadastrar = false;

class _SignInPageState extends State<SignInPage> {
  @override
  void initState() {
    super.initState();
    _emailController.text = "";
    podeCadastrar = false;
    _personalController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: SingleChildScrollView(
                child: Column(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-1.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                  child: Column(
                children: <Widget>[
                  Center(
                      child: Container(
                          margin: EdgeInsets.only(top: 50),
                          width: MediaQuery.of(context).size.width - 120,
                          child: Text(
                            'Preencha as informações abaixo',
                            style: TextStyle(
                                fontSize: 26,
                                fontWeight: FontWeight.bold,
                                color: Colors.white),
                            textAlign: TextAlign.center,
                          ))),
                  Container(
                      margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                      child: Column(
                        children: <Widget>[
                          Container(
                            height: 310,
                            width: MediaQuery.of(context).size.width - 70,
                            padding: EdgeInsets.all(20),
                            decoration: new BoxDecoration(
                              boxShadow: [
                                BoxShadow(
                                  color: Colors.grey,
                                  blurRadius: 15.0, // soften the shadow
                                  spreadRadius: 1.0, //extend the shadow
                                  offset: Offset(
                                    5.0, // Move to right 10  horizontally
                                    5.0, // Move to bottom 10 Vertically
                                  ),
                                )
                              ],
                              color: Colors.white,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(10.0),
                                  topRight: const Radius.circular(10.0),
                                  bottomLeft: const Radius.circular(10.0),
                                  bottomRight: const Radius.circular(10.0)),
                            ),
                            child: SingleChildScrollView(
                                child: Column(
                              children: <Widget>[
                                Form(
                                    key: _formKey7,
                                    child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: <Widget>[
                                          Text('E-mail da Hotmart',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                              textAlign: TextAlign.left),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 5, 0, 15),
                                              child: TextFormField(
                                                controller: _emailController,
                                                decoration: new InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.fromLTRB(
                                                            20, 15, 20, 15),
                                                    filled: true,
                                                    fillColor: Colors.grey[300],
                                                    border:
                                                        new OutlineInputBorder(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                        const Radius.circular(
                                                            25.0),
                                                      ),
                                                    ),
                                                    hintText:
                                                        'E-mail da Hotmart'),
                                                validator: (value) {
                                                  emailValid = RegExp(
                                                          r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                      .hasMatch(value);
                                                  if (value.isEmpty) {
                                                    return 'Digite seu e-mail';
                                                  }
                                                  return null;
                                                },
                                              )),
                                          Text('CPF/CNPJ',
                                              style: TextStyle(
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 16),
                                              textAlign: TextAlign.left),
                                          Padding(
                                              padding: EdgeInsets.fromLTRB(
                                                  0, 5, 0, 15),
                                              child: TextFormField(
                                                controller: _personalController,
                                                keyboardType:
                                                    TextInputType.number,
                                                decoration: new InputDecoration(
                                                    contentPadding:
                                                        EdgeInsets.fromLTRB(
                                                            20, 15, 20, 15),
                                                    filled: true,
                                                    fillColor: Colors.grey[300],
                                                    border:
                                                        new OutlineInputBorder(
                                                      borderRadius:
                                                          const BorderRadius
                                                              .all(
                                                        const Radius.circular(
                                                            25.0),
                                                      ),
                                                    ),
                                                    hintText: 'CPF/CNPJ'),
                                                validator: (value) {
                                                  if (value.isEmpty) {
                                                    return 'Digite o CPF/CNPJ';
                                                  }
                                                  if(CPF.isValid(value) || CNPJ.isValid(value))
                                                  {
                                                    cpfValid = true;
                                                  }
                                                  else{
                                                    cpfValid = false;
                                                  }
                                                  if (!cpfValid){
                                                    return "Digite CPF/CNPJ válido!";
                                                  }
                                                  return null;
                                                },
                                              )),
                                          Container(
                                              width: MediaQuery.of(context)
                                                      .size
                                                      .width -
                                                  100,
                                              height: 40,
                                              child: RaisedButton(
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                .circular(18.0),
                                                        side: BorderSide(
                                                            color: Colors
                                                                .orangeAccent)),
                                                onPressed: () {
                                                  if (_formKey7.currentState
                                                      .validate()) {
                                                    if (!emailValid) {
                                                      _onAlertButtonPressed(
                                                          context);
                                                    } else {
                                                      _performSignIn();
                                                    }
                                                  }
                                                },
                                                color: Colors.orangeAccent,
                                                textColor: Colors.white,
                                                child: Text("Continuar",
                                                    style: TextStyle(
                                                        fontSize: 18)),
                                              )),
                                        ])),
                              ],
                            )),
                          ),
                        ],
                      )),
                ],
              )))
        ]))));
  }

  void _performSignIn() {
     checkEmail().then((otherDate) {
      dbRef
          .reference()
          .child('Users/' + textToMd5(_emailController.text))
          .once()
          .then((DataSnapshot snapshot) {
        if (snapshot.value == null) {
          obj.emailHotmart = _emailController.text.replaceAll(' ', '');
          obj.personalInfo = _personalController.text;
          obj.uid = textToMd5(obj.emailHotmart);
           if (podeCadastrar) {
            navigateToPasswordPage(context);
           } else {
             _onUserNotFound(context);
           }
        } else
          return _onAlertEmailExists(context);
      });
    });
  }
}

Future checkEmail() async {
  var response = await http.post(Uri.encodeFull(url), headers: {
    "Authorization":
        "Basic ODFkMDQzMGQtNGJlMy00MzBjLTljNTYtZWM2ZWIwNTBmZjZmOjA5MjQyNGMxLTU2ZmItNDk1Yi04YjRiLWMzZWZiNGE5YzFhYg=="
  });
  var v = urlEmail + _emailController.text;
  if (response.statusCode == 200) {
    var jsonResponse = convert.jsonDecode(response.body);
    var getHistory = await http.get(Uri.encodeFull(v),
        headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
    var history = convert.jsonDecode(getHistory.body);
    if (history["data"].length == 0) {
      podeCadastrar = false;
    } else {
      podeCadastrar = true;
    }
  }
}

_onUserNotFound(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está incorreto ou ainda não possui cadastro na Hotmart.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onAlertButtonPressed(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está em formato incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onAlertEmailExists(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail já cadastrado",
    desc: "O e-mail informado já está cadastrado.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}
