import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:social_protect_app/editInfo.dart';
import 'package:social_protect_app/systemUser.dart';
import 'main.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'globals.dart';

class FillInfoPage extends StatefulWidget {
  FillInfoPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _FillInfoPageState createState() => _FillInfoPageState();
}

int validOffers = 0;
final dbRef = FirebaseDatabase.instance.reference();
bool emailValid = false;
bool instaFilled = false;
bool emailFilled = false;
bool telFilled = false;
bool canInsert = false;
final _instaController = TextEditingController();
final _emailController = TextEditingController();
final _telController = TextEditingController();
final _codeController = TextEditingController();
List<dynamic> ordersValid = new List();

class _FillInfoPageState extends State<FillInfoPage> {
  final _formKey32 = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _instaController.text = "";
    _emailController.text = "";
    _telController.text = "";
    _codeController.text = "";
    canInsert = false;
    verifyNumber();
  }

  @override
  Widget build(BuildContext context) {
    final bottom = MediaQuery.of(context).viewInsets.bottom;
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("img/BG-1.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: SingleChildScrollView(
                    child: Padding(
                        padding: EdgeInsets.only(bottom: bottom),
                        child: SingleChildScrollView(
                            child: Column(children: <Widget>[
                          Padding(
                            padding: EdgeInsets.only(top: 40),
                          ),
                          Container(
                            child: Column(children: <Widget>[
                              Container(
                                  height: 390,
                                  width: MediaQuery.of(context).size.width - 70,
                                  padding: EdgeInsets.fromLTRB(20, 0, 20, 10),
                                  decoration: new BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 15.0, // soften the shadow
                                        spreadRadius: 1.0, //extend the shadow
                                        offset: Offset(
                                          5.0, // Move to right 10  horizontally
                                          5.0, // Move to bottom 10 Vertically
                                        ),
                                      )
                                    ],
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(10.0),
                                        topRight: const Radius.circular(10.0),
                                        bottomLeft: const Radius.circular(10.0),
                                        bottomRight:
                                            const Radius.circular(10.0)),
                                  ),
                                  child: Column(children: <Widget>[
                                    Form(
                                        key: _formKey32,
                                        child: ListView(
                                            shrinkWrap: true,
                                            children: <Widget>[
                                              Text('Instagram',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.left),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 5, 0, 15),
                                                  child: TextFormField(
                                                    controller:
                                                        _instaController,
                                                    decoration:
                                                        new InputDecoration(
                                                            contentPadding:
                                                                EdgeInsets.fromLTRB(
                                                                    20,
                                                                    15,
                                                                    20,
                                                                    15),
                                                            filled: true,
                                                            fillColor: Colors
                                                                .grey[300],
                                                            border:
                                                                new OutlineInputBorder(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(
                                                                const Radius
                                                                        .circular(
                                                                    25.0),
                                                              ),
                                                            ),
                                                            hintText:
                                                                'Id do Instagram'),
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        instaFilled = false;
                                                      } else {
                                                        instaFilled = true;
                                                      }
                                                      return null;
                                                    },
                                                  )),
                                              Text('E-mail do Instagram',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.left),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 5, 0, 15),
                                                  child: TextFormField(
                                                    controller:
                                                        _emailController,
                                                    decoration:
                                                        new InputDecoration(
                                                            contentPadding:
                                                                EdgeInsets.fromLTRB(
                                                                    20,
                                                                    15,
                                                                    20,
                                                                    15),
                                                            filled: true,
                                                            fillColor: Colors
                                                                .grey[300],
                                                            border:
                                                                new OutlineInputBorder(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(
                                                                const Radius
                                                                        .circular(
                                                                    25.0),
                                                              ),
                                                            ),
                                                            hintText:
                                                                'user@socialprotect.com'),
                                                    validator: (value) {
                                                      emailValid = RegExp(
                                                              r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                          .hasMatch(value);
                                                      if (value.isEmpty) {
                                                        emailFilled = false;
                                                      } else {
                                                        emailFilled = true;
                                                      }
                                                      return null;
                                                    },
                                                  )),
                                              Text('Telefone do Instagram',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.left),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 5, 0, 15),
                                                  child: TextFormField(
                                                    controller: _telController,
                                                    keyboardType:
                                                        TextInputType.number,
                                                    decoration:
                                                        new InputDecoration(
                                                            contentPadding:
                                                                EdgeInsets.fromLTRB(
                                                                    20,
                                                                    15,
                                                                    20,
                                                                    15),
                                                            filled: true,
                                                            fillColor: Colors
                                                                .grey[300],
                                                            border:
                                                                new OutlineInputBorder(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(
                                                                const Radius
                                                                        .circular(
                                                                    25.0),
                                                              ),
                                                            ),
                                                            hintText:
                                                                'xx-xxxxx-xxxx'),
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        telFilled = false;
                                                      } else {
                                                        telFilled = true;
                                                      }
                                                      return null;
                                                    },
                                                  )),
                                              Text('Código da Hotmart',
                                                  style: TextStyle(
                                                      fontWeight:
                                                          FontWeight.bold,
                                                      fontSize: 16),
                                                  textAlign: TextAlign.left),
                                              Padding(
                                                  padding: EdgeInsets.fromLTRB(
                                                      0, 5, 0, 15),
                                                  child: TextFormField(
                                                    controller: _codeController,
                                                    decoration:
                                                        new InputDecoration(
                                                            contentPadding:
                                                                EdgeInsets.fromLTRB(
                                                                    20,
                                                                    15,
                                                                    20,
                                                                    15),
                                                            filled: true,
                                                            fillColor: Colors
                                                                .grey[300],
                                                            border:
                                                                new OutlineInputBorder(
                                                              borderRadius:
                                                                  const BorderRadius
                                                                      .all(
                                                                const Radius
                                                                        .circular(
                                                                    25.0),
                                                              ),
                                                            ),
                                                            hintText:
                                                                'Código da Hotmart'),
                                                    validator: (value) {
                                                      if (value.isEmpty) {
                                                        codeFilled = false;
                                                      } else {
                                                        codeFilled = true;
                                                      }
                                                      return null;
                                                    },
                                                  )),
                                            ])),
                                  ]))
                            ]),
                          ),
                          Container(
                            margin: EdgeInsets.only(top: 30),
                            height: 130,
                            width: 270,
                            padding: EdgeInsets.fromLTRB(5, 10, 5, 0),
                            decoration: new BoxDecoration(
                              border: Border.all(color: Colors.white),
                              color: Colors.transparent,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(5.0),
                                  topRight: const Radius.circular(5.0),
                                  bottomLeft: const Radius.circular(5.0),
                                  bottomRight: const Radius.circular(5.0)),
                            ),
                            child: Column(
                              children: <Widget>[
                                Text(
                                  'O e-mail e telefone são os mesmos utilizados em seu Instagram?',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Text(
                                  '_____________________________',
                                  style: TextStyle(
                                      fontSize: 16, color: Colors.white),
                                  textAlign: TextAlign.center,
                                ),
                                Container(
                                  margin: EdgeInsets.only(top: 10),
                                  child: Row(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: <Widget>[
                                      Container(
                                          width: 110,
                                          height: 28,
                                          child: RaisedButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 0),
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        18.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),
                                            onPressed: () {
                                              if (_formKey32.currentState
                                                  .validate()) {
                                                if (!instaFilled) {
                                                  _onInstaNotFilled(context);
                                                } else if (!emailFilled) {
                                                  _onEmailNotFilled(context);
                                                } else if (!emailValid) {
                                                  _onEmailIncorrect(context);
                                                } else if (!telFilled) {
                                                  _onTelNotFilled(context);
                                                } else if (!codeFilled) {
                                                  _onCodeNotFilled(context);
<<<<<<< HEAD
<<<<<<< HEAD
                                                } else
                                                if (canInsert)
                                                {
                                                  _onConfirmInfo(context);
                                                }
                                                else {
                                                  _onListFull(context);
                                                }
=======
                                                } 
                                                else {
                                                //if (canInsert) {
                                                  _onConfirmInfo(context);
                                                } 
                                                //else {
                                                 // _onListFull(context);
                                                //}
>>>>>>> 4a6137ffd33cfc064bd8255ff4acc0cc7c48dc79
=======
                                                } else
                                                if (canInsert)
                                                {
                                                  _onConfirmInfo(context);
                                                }
                                                else {
                                                  _onListFull(context);
                                                }
>>>>>>> ad9cd7d1bfd0fca008320e3fdedae6c51ca1b7c9
                                              }
                                            },
                                            color: Colors.grey[100],
                                            textColor: Colors.blue,
                                            child: Text("Sim, salvar",
                                                style: TextStyle(fontSize: 16)),
                                          )),
                                      Container(
                                          margin: EdgeInsets.only(left: 10),
                                          width: 110,
                                          height: 28,
                                          child: RaisedButton(
                                            padding: EdgeInsets.symmetric(
                                                horizontal: 0),
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        18.0),
                                                side: BorderSide(
                                                    color: Colors.blue)),
                                            onPressed: () {
                                              navigateToVerifyInfo1(context);
                                            },
                                            color: Colors.grey[100],
                                            textColor: Colors.blue,
                                            child: Text("Tenho dúvida",
                                                style: TextStyle(fontSize: 16)),
                                          )),
                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.only(bottom: 30),
                          )
                        ])))))));
  }
}

Future verifyNumber() async {
  var response = await http.post(Uri.encodeFull(url), headers: {
    "Authorization":
        "Basic ODFkMDQzMGQtNGJlMy00MzBjLTljNTYtZWM2ZWIwNTBmZjZmOjA5MjQyNGMxLTU2ZmItNDk1Yi04YjRiLWMzZWZiNGE5YzFhYg=="
  });
  var jsonResponse = convert.jsonDecode(response.body);
  var getHistory = await http.get(Uri.encodeFull(urlHistory),
      headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
  var history = convert.jsonDecode(getHistory.body);

  var getOffers = await http.get(Uri.encodeFull(urlOffers),
      headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
  offers = convert.jsonDecode(getOffers.body);
  validOffers = 0;
  for (int i = 0; i < history["data"].length; i++) {
    if (history["data"][i]["buyer"]["email"] == obj.emailHotmart) {
      verifyDate(
          history["data"][i],
          history["data"][i]["purchase"]["approvedDate"],
          history["data"][i]["offer"]["key"]);
    }
  }
  dbRef
      .reference()
      .child('Users/' + textToMd5(obj.emailHotmart))
      .once()
      .then((DataSnapshot snapshot) {
    if (snapshot != null) {
      var tt = new Map<String, dynamic>.from(snapshot.value);
      SystemUser usuario = new SystemUser.fromJson(tt);
      var num = 0;
      if (usuario.instas == null) {
        num = 0;
      } else {
        num = usuario.instas.length;
      }
      if (num < validOffers) {
        canInsert = true;
      }
    }
  });
}

verifyDate(order, approvalDate, key) {
  var date = new DateTime.fromMillisecondsSinceEpoch(
          int.parse(approvalDate.toString()))
      .toLocal();
  String signature = "";
  for (int i = 0; i < offers["data"].length; i++) {
    if (offers["data"][i]["key"] == key) {
      signature = offers["data"][i]["planPeriodicity"];
    }
  }

  if (signature == "QUARTERLY") {
    date = date.add(new Duration(days: 90));
  } else if (signature == "BIANNUAL") {
    date = date.add(new Duration(days: 180));
  } else if (signature == "ANNUAL") {
    date = date.add(new Duration(days: 365));
  } else if (signature == "MONTHLY") {
    date = date.add(new Duration(days: 31));
  }
  if (date.isAfter(DateTime.now())) {
    validOffers++;
    ordersValid.add(order);
  }
}

bool containsCurrentOrder(codeHotmart) {
  List<dynamic> list = ordersValid.toList();
  for (int i = 0; i < list.length; i++) {
    if (list[i]["purchase"]["transaction"] == codeHotmart) {
      return true;
    }
  }
  return false;
}

void _performInfo(context) {
  Navigator.pop(context);
  user.idInstagram = _instaController.text;
  user.emailInstagram = _emailController.text;
  user.emailHotmart = obj.emailHotmart;
  user.telInstagram = _telController.text;
  user.codeHotmart = _codeController.text;
  user.expirationDate = "";

  if(containsCurrentOrder(user.codeHotmart) == false){
    return _onHPExpired(context);
  }
  
  dbRef
      .reference()
      .child('Users/' + textToMd5(obj.emailHotmart))
      .once()
      .then((DataSnapshot snapshot) {
    if (snapshot != null) {
      var jaTem = false;
      var taUsando = false;
      var map = new Map<String, dynamic>.from(snapshot.value);
      SystemUser usuario = new SystemUser.fromJson(map);
      if (usuario.instas != null) {
        List<dynamic> instas = usuario.instas.values.toList();
        for (int i = 0; i < usuario.instas.length; i++) {
          if (instas[i]["idInstagram"] == user.idInstagram) {
            jaTem = true;
          }
          if(instas[i]["codeHotmart"] == user.codeHotmart){
            taUsando=true;
          }
        }
      }
      if(jaTem){
        _onInstaExists(context);
      }
      else if(taUsando){
        _onHPExists(context);
      }
      else{
        DatabaseReference postsRef = dbRef
            .child('Users/' + textToMd5(obj.emailHotmart))
            .child('instas/');
        postsRef.push().set(user.toJson());
        navigateToFactorsPage(context);
      }
    }
    else{
       DatabaseReference postsRef = dbRef
            .child('Users/' + textToMd5(obj.emailHotmart))
            .child('instas/');
        postsRef.push().set(user.toJson());
        navigateToFactorsPage(context);
    }
  });
}

_onConfirmInfo(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Adição de Instagram",
    desc:
        "Tem certeza que o Id do Instagram está correto? Ele não poderá ser alterado depois.",
    buttons: [
      DialogButton(
        child: Text(
          "Sim",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => _performInfo(context),
        width: 120,
      ),
      DialogButton(
        child: Text(
          "Não",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

<<<<<<< HEAD
<<<<<<< HEAD
=======
>>>>>>> ad9cd7d1bfd0fca008320e3fdedae6c51ca1b7c9
_onInstaExists(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Instagram já cadastrado!",
    desc:
        "O Id de Instagram informado já foi cadastrado nesta conta.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onHPExists(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código Hotmart já cadastrado!",
    desc:
        "O código Hotmart informado já foi cadastrado anteriormente.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onHPExpired(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código Hotmart informado inválido!",
    desc:
        "O código Hotmart informado está inválido.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      ),
    ],
  ).show();
}

_onListFull(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Máximo de contas atingido!",
    desc:
        "O número máximo de contas de Instagram criadas foi atingido para esta conta.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => navigateToSelectInstaPage(context),
        width: 120,
      ),
    ],
  ).show();
}
<<<<<<< HEAD
=======
// _onListFull(context) {
//   Alert(
//     context: context,
//     type: AlertType.error,
//     title: "Máximo de contas atingido!",
//     desc:
//         "O número máximo de contas de Instagram criadas foi atingido para esta conta.",
//     buttons: [
//       DialogButton(
//         child: Text(
//           "Entendi",
//           style: TextStyle(color: Colors.white, fontSize: 20),
//         ),
//         onPressed: () => navigateToSelectInstaPage(context),
//         width: 120,
//       ),
//     ],
//   ).show();
// }
>>>>>>> 4a6137ffd33cfc064bd8255ff4acc0cc7c48dc79
=======
>>>>>>> ad9cd7d1bfd0fca008320e3fdedae6c51ca1b7c9

_onEmailIncorrect(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está em formato incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onInstaNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "ID do Instagram em branco",
    desc: "Preencha o ID do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onEmailNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail do Instagram em branco",
    desc: "Preencha o e-mail do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onTelNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Telefone do Instagram em branco",
    desc: "Preencha o telefone do Instagram para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onCodeNotFilled(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Código da Hotmart em branco",
    desc: "Preencha o código da Hotmart para prosseguir.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}
