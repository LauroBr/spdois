import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class SubscribePagePJ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            child: Column(children: <Widget>[
      Container(
          padding: EdgeInsets.fromLTRB(20, 30, 20, 15),
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("img/BG-1.png"),
              fit: BoxFit.fill,
            ),
          ),
          child: SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      height: 80,
                      width: 120,
                      margin: EdgeInsets.only(top: 10),
                      padding: EdgeInsets.only(top: 5),
                      decoration: new BoxDecoration(
                        border: Border.all(color: Colors.white),
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(5.0),
                            topRight: const Radius.circular(5.0),
                            bottomLeft: const Radius.circular(5.0),
                            bottomRight: const Radius.circular(5.0)),
                      ),
                      child: Column(children: <Widget>[
                        Image.asset(
                          'img/group-w.png',
                          height: 45,
                          width: 55,
                        ),
                        AccountTypePJ()
                      ])),
                ],
              ),
              Row(
                children: <Widget>[
                  Flexible(
                      child: Container(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 20),
                          child: Text(
                            'Contrate agora o nosso serviço e mantenha a sua rede social segura',
                            style: TextStyle(
                                fontSize: 20,
                                color: Colors.white,
                                fontWeight: FontWeight.bold),
                            textAlign: TextAlign.center,
                          ))),
                ],
              ),
              Row(
                children: <Widget>[
                  Sub3Months(),
                  Padding(
                    padding: EdgeInsets.only(left: 20),
                  ),
                  Sub6Months(),
                ],
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Row(
                children: <Widget>[Sub12Months()],
              ),
              Padding(
                padding: EdgeInsets.only(top: 20),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Container(
                      width: 220,
                      child: GestureDetector(
                        onTap: () {
                          goToSite();
                        },
                        child: Image.asset(
                          'img/Logo-3.png',
                        ),
                      )),
                ],
              ),
            ],
          ))),
    ])));
  }
}

goToSite() async {
  const url = 'http://socialprotect.com.br/';
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

class AccountTypePJ extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        margin: EdgeInsets.only(top: 0),
        decoration: new BoxDecoration(
          borderRadius: new BorderRadius.all(Radius.circular(100)),
        ),
        child: Container(
            padding: EdgeInsets.symmetric(vertical: 2, horizontal: 4),
            color: Colors.orangeAccent,
            child: Text(
              'Conta Empresa',
              style:
                  TextStyle(color: Colors.white, fontWeight: FontWeight.bold),
            )));
  }
}

class Sub3Months extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        height: 180,
        width: MediaQuery.of(context).size.width / 2 - 30,
        decoration: new BoxDecoration(
          color: Colors.grey[100],
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(10.0),
              topRight: const Radius.circular(10.0),
              bottomLeft: const Radius.circular(10.0),
              bottomRight: const Radius.circular(10.0)),
        ),
        child: Column(
          children: <Widget>[
            Flexible(
                flex: 2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 4 - 20,
                        child: Text("3 Meses",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold))),
                    Container(
                        width: MediaQuery.of(context).size.width / 4 - 30,
                        height: 32,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Image.asset(
                                'img/3m-Btn.png',
                                width: 35,
                                height: 32,
                              )
                            ])),
                  ],
                )),
            Padding(
              padding: EdgeInsets.only(top: 3),
            ),
            Expanded(
                flex: 4,
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                          flex: 4,
                          child: Text(
                            'Valor Total: R\$387.00 + 3 Bônus Inclusos – Para cadastro de pessoas jurídicas com CNPJ.',
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                            textAlign: TextAlign.justify,
                          ))
                    ],
                  )
                ]))),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        height: 20,
                        width: 90,
                        child: RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.orangeAccent)),
                          onPressed: () {
                            buySignature();
                          },
                          color: Colors.orangeAccent,
                          textColor: Colors.white,
                          child:
                              Text("Contratar", style: TextStyle(fontSize: 12)),
                        ))
                  ],
                )))
          ],
        ));
  }

  buySignature() async {
    const url = 'https://pay.hotmart.com/H20394868R?off=irsfbbbf';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class Sub6Months extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        height: 180,
        width: MediaQuery.of(context).size.width / 2 - 30,
        decoration: new BoxDecoration(
          color: Colors.grey[100],
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(10.0),
              topRight: const Radius.circular(10.0),
              bottomLeft: const Radius.circular(10.0),
              bottomRight: const Radius.circular(10.0)),
        ),
        child: Column(
          children: <Widget>[
            Flexible(
                flex: 2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 4 - 20,
                        child: Text("6 Meses",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold))),
                    Container(
                        width: MediaQuery.of(context).size.width / 4 - 30,
                        height: 32,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Image.asset(
                                'img/6m-Btn.png',
                                width: 35,
                                height: 32,
                              )
                            ])),
                  ],
                )),
            Padding(
              padding: EdgeInsets.only(top: 3),
            ),
            Expanded(
                flex: 4,
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                          flex: 4,
                          child: Text(
                            '[Economia de R\$ 129.00] - Valor Total: R\$645.00 + 3 Bônus Inclusos – Para cadastro de pessoas jurídicas com CNPJ.',
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                          ))
                    ],
                  )
                ]))),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        height: 20,
                        width: 90,
                        child: RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.orangeAccent)),
                          onPressed: () {
                            buySignature();
                          },
                          color: Colors.orangeAccent,
                          textColor: Colors.white,
                          child:
                              Text("Contratar", style: TextStyle(fontSize: 12)),
                        ))
                  ],
                )))
          ],
        ));
  }

  buySignature() async {
    const url = 'https://pay.hotmart.com/H20394868R?off=jlyjsdac';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}

class Sub12Months extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: EdgeInsets.fromLTRB(10, 10, 10, 0),
        height: 170,
        width: MediaQuery.of(context).size.width - 40,
        decoration: new BoxDecoration(
          boxShadow: [
            BoxShadow(
              color: Colors.grey,
              blurRadius: 15.0, // soften the shadow
              spreadRadius: 1.0, //extend the shadow
              offset: Offset(
                5.0, // Move to right 10  horizontally
                5.0, // Move to bottom 10 Vertically
              ),
            )
          ],
          color: Colors.grey[100],
          borderRadius: new BorderRadius.only(
              topLeft: const Radius.circular(10.0),
              topRight: const Radius.circular(10.0),
              bottomLeft: const Radius.circular(10.0),
              bottomRight: const Radius.circular(10.0)),
        ),
        child: Column(
          children: <Widget>[
            Flexible(
                flex: 2,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                        width: MediaQuery.of(context).size.width / 2 - 20,
                        child: Text("12 Meses",
                            style: TextStyle(
                                fontSize: 18, fontWeight: FontWeight.bold))),
                    Container(
                        width: MediaQuery.of(context).size.width / 2 - 40,
                        height: 32,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Image.asset(
                                'img/12m-Btn.png',
                                width: 35,
                                height: 32,
                              )
                            ])),
                  ],
                )),
            Padding(
              padding: EdgeInsets.only(top: 3),
            ),
            Expanded(
                flex: 4,
                child: SingleChildScrollView(
                    child: Column(children: <Widget>[
                  Row(
                    children: <Widget>[
                      Flexible(
                          flex: 4,
                          child: Text(
                            '[Economia de R\$ 387.00] - Valor Total: R\$1161.00 + 3 Bônus Inclusos – Para cadastro de pessoas jurídicas com CNPJ. O plano anual é o mais indicado, ao assiná-lo, você garante 3 meses de economia. Tenha seu perfil protegido o ano todo pagando muito menos!',
                            style: TextStyle(color: Colors.grey, fontSize: 15),
                            textAlign: TextAlign.justify,
                          ))
                    ],
                  )
                ]))),
            Flexible(
                flex: 1,
                fit: FlexFit.tight,
                child: Container(
                    child: Row(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: <Widget>[
                    Container(
                        margin: EdgeInsets.only(top: 5),
                        height: 20,
                        width: 90,
                        child: RaisedButton(
                          shape: new RoundedRectangleBorder(
                              borderRadius: new BorderRadius.circular(18.0),
                              side: BorderSide(color: Colors.orangeAccent)),
                          onPressed: () {
                            buySignature();
                          },
                          color: Colors.orangeAccent,
                          textColor: Colors.white,
                          child:
                              Text("Contratar", style: TextStyle(fontSize: 12)),
                        ))
                  ],
                )))
          ],
        ));
  }

  buySignature() async {
    const url = 'https://pay.hotmart.com/H20394868R?off=3evfy3kj';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }
}
