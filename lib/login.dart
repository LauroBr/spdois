import 'package:flutter/material.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'package:social_protect_app/systemUser.dart';
import 'package:url_launcher/url_launcher.dart';
import 'main.dart';
import 'package:firebase_database/firebase_database.dart';
import 'globals.dart';

class LoginPage extends StatefulWidget {
  LoginPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _LoginPageState createState() => _LoginPageState();
}

bool loginValid = false;
bool emailValid = false;
final _formKey4 = GlobalKey<FormState>();
final dbRef = FirebaseDatabase.instance.reference();

class _LoginPageState extends State<LoginPage> {
  final _usernameController = TextEditingController();
  final _passwordController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Column(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-4.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                child: Column(children: <Widget>[
            Center( 
              child: Container(
                margin: EdgeInsets.only(top: 15),
                height: 130,
                width: MediaQuery.of(context).size.width-100,
                child: Image.asset(
                  'img/Logo1.png',
                )
            )),
            Center(
              child: Container(
                margin: EdgeInsets.fromLTRB(35, 5, 35, 0),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 340,
                      width: MediaQuery.of(context).size.width-80,
                      padding: EdgeInsets.all(20),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 15.0, // soften the shadow
                            spreadRadius: 1.0, //extend the shadow
                            offset: Offset(
                              5.0, // Move to right 10  horizontally
                              5.0, // Move to bottom 10 Vertically
                            ),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: SingleChildScrollView(
                          child: Column(
                        children: <Widget>[
                          Form(
                              key: _formKey4,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('E-mail',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        textAlign: TextAlign.left),
                                    Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 5, 0, 15),
                                        child: TextFormField(
                                          controller: _usernameController,
                                          decoration: new InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 15, 20, 15),
                                              filled: true,
                                              fillColor: Colors.grey[300],
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(25.0),
                                                ),
                                              ),
                                              hintText:
                                                  'user@socialprotect.com'),
                                          validator: (value) {
                                            emailValid = RegExp(
                                                    r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
                                                .hasMatch(value);
                                            if (value.isEmpty) {
                                              return 'Digite seu e-mail';
                                            }
                                            return null;
                                          },
                                        )),
                                    Text('Senha',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        textAlign: TextAlign.left),
                                    Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 5, 0, 15),
                                        child: TextFormField(
                                          controller: _passwordController,
                                          obscureText: true,
                                          decoration: new InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 15, 20, 15),
                                              filled: true,
                                              fillColor: Colors.grey[300],
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(25.0),
                                                ),
                                              ),
                                              hintText: 'Digite sua senha'),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Digite sua senha';
                                            }
                                            return null;
                                          },
                                        )),
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 40,
                                        child: RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      18.0),
                                              side: BorderSide(
                                                  color: Colors.orangeAccent)),
                                          onPressed: () {
                                            if (_formKey4.currentState
                                                .validate()) {
                                              if (!emailValid) {
                                                _onAlertEmailError(context);
                                              } else {
                                                _performLogin();
                                              }
                                            }
                                          },
                                          color: Colors.orangeAccent,
                                          textColor: Colors.white,
                                          child: Text("Acessar",
                                              style: TextStyle(fontSize: 18)),
                                        )),
                                    Container(
                                        margin:
                                            EdgeInsets.fromLTRB(30, 10, 30, 0),
                                        width:
                                            MediaQuery.of(context).size.width,
                                        height: 30,
                                        child: RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      18.0),
                                              side: BorderSide(
                                                  color: Colors.blue)),
                                          onPressed: () {
                                            navigateToConfirmInfoPage(context);
                                          },
                                          color: Colors.grey[100],
                                          textColor: Colors.blue,
                                          child: Text("Esqueceu sua senha",
                                              style: TextStyle(fontSize: 14)),
                                        )),
                                  ])),
                        ],
                      )),
                    ),
                  ],
                )),
            ),
            Container(
                margin: EdgeInsets.only(top: 10),
                child: Padding(
                    padding: EdgeInsets.fromLTRB(30,10,30,0),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 5),
                        ),
                        Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              Text("Já se inscreveu?",
                                  style: TextStyle(fontSize: 16)),
                              Container(
                                height: 25,
                                margin: EdgeInsets.only(left: 20),
                                child: RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(color: Colors.blue)),
                                  onPressed: () {
                                    navigateToSignInPage(context);
                                  },
                                  color: Colors.grey[100],
                                  textColor: Colors.blue,
                                  child: Text("Cadastre-se",
                                      style: TextStyle(fontSize: 14)),
                                ),
                              ),
                            ]),
                        Text("______________________________________",
                            style: TextStyle(fontSize: 14)),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            InkWell(
                              onTap: () {
                                goTo1();
                              },
                              child: Text("Quem Somos  |",
                                  style: TextStyle(fontSize: 18)),
                            ),
                            InkWell(
                              onTap: () {
                                goTo2();
                              },
                              child: Text("  Termos de Serviço",
                                  style: TextStyle(fontSize: 18)),
                            )
                          ],
                        )
                      ],
                    ))),
              
        ])))])));
  }

 goTo1() async {
    const url = 'http://socialprotect.com.br/quemsomos';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  goTo2() async {
    const url = 'http://socialprotect.com.br/termoseservicos';
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      throw 'Could not launch $url';
    }
  }

  void _performLogin() {
    String email = _usernameController.text.replaceAll(' ', '');
    String password = _passwordController.text;

    dbRef
        .reference()
        .child('Users/' + textToMd5(email))
        .once()
        .then((DataSnapshot snapshot) {
           if(snapshot.value == null) return _onAlertIncorrectUser(context);
      var map = new Map<String, dynamic>.from(snapshot.value);
      obj = new SystemUser.fromJson(map);
      loginValid=false;
      if (textToMd5(obj.emailHotmart) == textToMd5(email)) {
        if (obj.password == textToMd5(password)) {
          loginValid = true;
          user.emailHotmart = obj.emailHotmart;
        } else {
          _onAlertIncorrectPassword(context);
        }
      } else {
        _onAlertIncorrectUser(context);
      }
      if (loginValid) {
        navigateToSelectInstaPage(context);
      }
    });
  }
}

_onAlertIncorrectPassword(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Senha incorreta",
    desc: "A senha parece estar incorreta.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onAlertIncorrectUser(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Usuário incorreto",
    desc: "O usuário parece estar incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

_onAlertEmailError(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "E-mail inválido",
    desc: "O e-mail informado está em formato incorreto.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}
