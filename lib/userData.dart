class UserData {
  String type;
  String idInstagram;
  String emailInstagram;
  String expirationDate;
  String codeHotmart;
  String telInstagram;
  String emailHotmart;

  UserData(this.type, this.idInstagram, this.emailInstagram,
       this.expirationDate, this.codeHotmart,this.telInstagram, this.emailHotmart);

  UserData.empty();

  UserData.fromJson2(Map data){
    idInstagram = data['idInstagram'];
  }

  UserData.fromJson(Map<dynamic, dynamic> json)
      : type = json['type'],
        idInstagram = json['idInstagram'],
        emailInstagram = json['emailInstagram'],
        expirationDate = json['expirationDate'],
        codeHotmart = json['codeHotmart'],
        telInstagram = json['telInstagram'],
        emailHotmart = json['emailHotmart'];

  Map<String, dynamic> toJson() =>
    {
      'type': type,
      'idInstagram': idInstagram,
      'emailInstagram': emailInstagram,
      'expirationDate': expirationDate,
      'codeHotmart': codeHotmart,
      'telInstagram': telInstagram,
      'emailHotmart': emailHotmart      
    };
}
