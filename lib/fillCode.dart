import 'package:flutter/material.dart';
import 'package:social_protect_app/globals.dart';
import 'main.dart';
import 'package:firebase_database/firebase_database.dart';

class FillCodePage extends StatefulWidget {
  FillCodePage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _FillCodePageState createState() => _FillCodePageState();
}

final dbRef = FirebaseDatabase.instance.reference();

final _formKey2 = GlobalKey<FormState>();
final _factorController = TextEditingController();

class _FillCodePageState extends State<FillCodePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Column(
          children: <Widget>[
            Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("img/BG-1.png"),
                    fit: BoxFit.fill,
                  ),
                ),
                child: Column(children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 50),
                      padding:
                          EdgeInsets.symmetric(horizontal: 60, vertical: 20),
                      child: Text(
                        'Preencha o código do Instagram abaixo:',
                        style: TextStyle(
                            fontSize: 26,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      )),
                  Container(
                      child: Column(
                    children: <Widget>[
                      Container(
                        height: 340,
                        width: 350,
                        padding: EdgeInsets.all(20),
                        decoration: new BoxDecoration(
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey,
                              blurRadius: 15.0, // soften the shadow
                              spreadRadius: 1.0, //extend the shadow
                              offset: Offset(
                                5.0, // Move to right 10  horizontally
                                5.0, // Move to bottom 10 Vertically
                              ),
                            )
                          ],
                          color: Colors.white,
                          borderRadius: new BorderRadius.only(
                              topLeft: const Radius.circular(10.0),
                              topRight: const Radius.circular(10.0),
                              bottomLeft: const Radius.circular(10.0),
                              bottomRight: const Radius.circular(10.0)),
                        ),
                        child: SingleChildScrollView(
                            child: Column(
                          children: <Widget>[
                            Form(
                                key: _formKey2,
                                child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: <Widget>[
                                      Padding(
                                          padding:
                                              EdgeInsets.fromLTRB(50, 5, 50, 15),
                                          child: TextFormField(
                                            controller: _factorController,
                                            style: new TextStyle(
                                              fontSize: 24,
                                              color: Colors.blue,
                                            ),
                                            keyboardType: TextInputType.number,
                                            maxLines: 5,
                                            decoration: new InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      40, 15, 40, 15),
                                              filled: true,
                                              fillColor: Colors.grey[300],
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(25.0),
                                                ),
                                              ),
                                            ),
                                            validator: (value) {
                                              if (value.isEmpty) {
                                                return 'Digite seu código';
                                              }
                                              return null;
                                            },
                                          )),
                                      Container(
                                          width:
                                              MediaQuery.of(context).size.width,
                                          height: 40,
                                          child: RaisedButton(
                                            shape: new RoundedRectangleBorder(
                                                borderRadius:
                                                    new BorderRadius.circular(
                                                        18.0),
                                                side: BorderSide(
                                                    color:
                                                        Colors.orangeAccent)),
                                            onPressed: () {
                                              if (_formKey2.currentState
                                                  .validate()) {
                                                    _performCode();
                                              }
                                            },
                                            color: Colors.orangeAccent,
                                            textColor: Colors.white,
                                            child: Text("Avançar",
                                                style: TextStyle(fontSize: 18)),
                                          )),
                                    ])),
                          ],
                        )),
                      ),
                    ],
                  )),
                ]))
          ],
        )));
  }

  void _performCode() {
    // user.factorsCode = _factorController.text;
    // user.factorsCode = (removerCaracteresEspeciais(user.factorsCode));
    user.emailHotmart = obj.emailHotmart;
    dbRef.child('Insta/'+ textToMd5(obj.emailHotmart)).set(user.toJson());
    navigateToFillInfoPage(context);
  }

  removerCaracteresEspeciais(texto){
    if(texto.indexOf(",")>=0 || texto.indexOf(" ")>=0)
       return removerCaracteresEspeciais(texto.replaceAll(",", "").replaceAll(" ", ""));
    return texto;
}

}
