class SystemUser {
  String uid; //personal+codeHotmart
  String emailHotmart;
  String personalInfo;
  String password;
  String tempPassword;
  bool activeSession;
  Map<dynamic, dynamic> instas;
 
  SystemUser(this.uid, this.emailHotmart, this.personalInfo,
  this.password,this.tempPassword, this.activeSession, this.instas);
  
  SystemUser.empty();

  SystemUser.fromJson(Map<String, dynamic> json)
      : emailHotmart = json['emailHotmart'],
        personalInfo = json['personalInfo'],
        password = json['password'],
        tempPassword = json['tempPassword'],
        activeSession = json['activeSession'],
        instas = json['instas'];

  Map<String, dynamic> toJson() =>
    {
      'emailHotmart': emailHotmart,
      'personalInfo': personalInfo,
      'password': password,
      'tempPassword':tempPassword,
      'activeSession':activeSession,
      'instas':instas
    };
}