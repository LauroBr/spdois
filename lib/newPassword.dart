import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'main.dart';
import 'globals.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class NewPasswordPage extends StatefulWidget {
  NewPasswordPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _NewPasswordPageState createState() => _NewPasswordPageState();
}

final dbRef = FirebaseDatabase.instance.reference();
final _passController = TextEditingController();
final _confirmPassController = TextEditingController();

class _NewPasswordPageState extends State<NewPasswordPage> {
  final _formKey5 = GlobalKey<FormState>();

  @override
  void initState() {
    super.initState();
    _passController.text = "";
    _confirmPassController.text = "";
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
         child: SingleChildScrollView(
            child: Column(
             children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                decoration: BoxDecoration(
                  image: DecorationImage(
                    image: AssetImage("img/BG-1.png"),
                    fit: BoxFit.fill,
                  )),
                child: SingleChildScrollView(
            child: Column(
             children: <Widget>[
           Center( 
             child: Container(
                margin: EdgeInsets.only(top: 40),
                width: MediaQuery.of(context).size.width-120,
                child: Text(
                  'Cadastre sua nova senha',
                  style: TextStyle(
                      fontSize: 26,
                      fontWeight: FontWeight.bold,
                      color: Colors.white),
                  textAlign: TextAlign.center,
                ))),
            Container(
                margin: EdgeInsets.fromLTRB(35, 20, 35, 0),
                child: Column(
                  children: <Widget>[
                    Container(
                      height: 310,
                      width: MediaQuery.of(context).size.width-70,
                      padding: EdgeInsets.all(20),
                      decoration: new BoxDecoration(
                        boxShadow: [
                          BoxShadow(
                            color: Colors.grey,
                            blurRadius: 15.0, // soften the shadow
                            spreadRadius: 1.0, //extend the shadow
                            offset: Offset(
                              5.0, // Move to right 10  horizontally
                              5.0, // Move to bottom 10 Vertically
                            ),
                          )
                        ],
                        color: Colors.white,
                        borderRadius: new BorderRadius.only(
                            topLeft: const Radius.circular(10.0),
                            topRight: const Radius.circular(10.0),
                            bottomLeft: const Radius.circular(10.0),
                            bottomRight: const Radius.circular(10.0)),
                      ),
                      child: SingleChildScrollView(
                          child: Column(
                        children: <Widget>[
                          Form(
                              key: _formKey5,
                              child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Text('Nova Senha',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        textAlign: TextAlign.left),
                                    Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 5, 0, 15),
                                        child: TextFormField(
                                          controller: _passController,
                                          obscureText: true,
                                          decoration: new InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 15, 20, 15),
                                              filled: true,
                                              fillColor: Colors.grey[300],
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(25.0),
                                                ),
                                              ),
                                              hintText: 'Digite a senha'),
                                          validator: (value) {
                                            // passValid2 = RegExp(
                                            //         r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$')
                                            //     .hasMatch(value);
                                            if (value.isEmpty) {
                                              return 'Digite sua senha';
                                            }
                                            return null;
                                          },
                                        )),
                                    Text('Confirmar Nova Senha',
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 16),
                                        textAlign: TextAlign.left),
                                    Padding(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 5, 0, 15),
                                        child: TextFormField(
                                          controller: _confirmPassController,
                                          obscureText: true,
                                          decoration: new InputDecoration(
                                              contentPadding:
                                                  EdgeInsets.fromLTRB(
                                                      20, 15, 20, 15),
                                              filled: true,
                                              fillColor: Colors.grey[300],
                                              border: new OutlineInputBorder(
                                                borderRadius:
                                                    const BorderRadius.all(
                                                  const Radius.circular(25.0),
                                                ),
                                              ),
                                              hintText: 'Confirme a senha'),
                                          validator: (value) {
                                            if (value.isEmpty) {
                                              return 'Digite sua senha';
                                            }
                                            return null;
                                          },
                                        )),
                                    Container(
                                        width:
                                            MediaQuery.of(context).size.width -100,
                                        height: 40,
                                        child: RaisedButton(
                                          shape: new RoundedRectangleBorder(
                                              borderRadius:
                                                  new BorderRadius.circular(
                                                      18.0),
                                              side: BorderSide(
                                                  color: Colors.orangeAccent)),
                                          onPressed: () {
                                            if (_formKey5.currentState.validate() && _passController.text == _confirmPassController.text) {
                                              //if (passValid2) {
                                                _performNewPassword();
                                              // } else {
                                              //   _onAlertPassError(context);
                                              //   _passController.text = "";
                                              //   _confirmPassController.text = "";
                                              // }
                                            } else {
                                              _onAlertButtonPressed(context);
                                                _passController.text = "";
                                                _confirmPassController.text = "";
                                            }
                                          },
                                          color: Colors.orangeAccent,
                                          textColor: Colors.white,
                                          child: Text("Salvar",
                                              style: TextStyle(fontSize: 18)),
                                        )),
                                  ])),
                        ],
                      )),
                    ),
                  ],
                )),
          ],
        )))]))));
  }

  void _performNewPassword() {
    obj.password = textToMd5(_passController.text);
    dbRef.child('Users/' + textToMd5(obj.emailHotmart)).set(obj.toJson());
    navigateToLoginPage(context);
  }
}

_onAlertButtonPressed(context) {
  Alert(
    context: context,
    type: AlertType.error,
    title: "Senhas Inconsistentes",
    desc: "A senha deve ser confirmada.",
    buttons: [
      DialogButton(
        child: Text(
          "Entendi",
          style: TextStyle(color: Colors.white, fontSize: 20),
        ),
        onPressed: () => Navigator.pop(context),
        width: 120,
      )
    ],
  ).show();
}

// _onAlertPassError(context) {
//   Alert(
//     context: context,
//     type: AlertType.error,
//     title: "Senha Fraca",
//     desc:
//         "A senha deve ser conter no mínimo 8 caracteres sendo ao menos 1 letra maiúscula, 1 letra minúscula, 1 número e 1 símbolo especial.",
//     buttons: [
//       DialogButton(
//         child: Text(
//           "ENTENDI",
//           style: TextStyle(color: Colors.white, fontSize: 20),
//         ),
//         onPressed: () => Navigator.pop(context),
//         width: 120,
//       )
//     ],
//   ).show();
// }
