import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:social_protect_app/systemUser.dart';
import 'package:social_protect_app/userData.dart';
import 'main.dart';
import 'package:http/http.dart' as http;
import 'dart:convert' as convert;
import 'globals.dart';
//import 'package:back_button_interceptor/back_button_interceptor.dart';

class SelectInstaPage extends StatefulWidget {
  SelectInstaPage({Key key, this.title}) : super(key: key);
  final String title;

  @override
  _SelectInstaPageState createState() => _SelectInstaPageState();
}

final dbRef = FirebaseDatabase.instance.reference();
List<Widget> list = new List();
int validOffers = 0;

class _SelectInstaPageState extends State<SelectInstaPage> {
  @override
  void initState() {
    super.initState();
    list.clear();
    listMyInstas();
    //BackButtonInterceptor.add(myInterceptor);
  }

  // @override
  // void dispose() {
  //   BackButtonInterceptor.remove(myInterceptor);
  //   super.dispose();
  // }

  // bool myInterceptor(bool stopDefaultButtonEvent) {
  //   navigateToLoginPage(context);
  //   return true;
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        resizeToAvoidBottomPadding: false,
        body: Center(
            child: Column(children: <Widget>[
          Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage("img/BG-1.png"),
                  fit: BoxFit.fill,
                ),
              ),
              child: SingleChildScrollView(
                child: Column(children: <Widget>[
                  Container(
                      margin: EdgeInsets.only(top: 50),
                      padding:
                          EdgeInsets.symmetric(horizontal: 100, vertical: 20),
                      child: Text(
                        'Selecione o seu plano',
                        style: TextStyle(
                            fontSize: 20,
                            fontWeight: FontWeight.bold,
                            color: Colors.white),
                        textAlign: TextAlign.center,
                      )),
                  Container(
                      height: 500.0,
                      width: 300,
                      child: ListView(children: <Widget>[
                        Container(
                            margin: EdgeInsets.only(top: 10),
                            padding: EdgeInsets.all(15),
                            decoration: new BoxDecoration(
                              border: Border.all(color: Colors.white),
                              color: Colors.transparent,
                              borderRadius: new BorderRadius.only(
                                  topLeft: const Radius.circular(5.0),
                                  topRight: const Radius.circular(5.0),
                                  bottomLeft: const Radius.circular(5.0),
                                  bottomRight: const Radius.circular(5.0)),
                            ),
                            child: Column(children: <Widget>[
                              Container(
                                  height: 150,
                                  width: 150,
                                  padding: EdgeInsets.fromLTRB(5, 35, 5, 5),
                                  margin: EdgeInsets.only(bottom: 20),
                                  decoration: new BoxDecoration(
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey,
                                        blurRadius: 15.0, // soften the shadow
                                        spreadRadius: 1.0, //extend the shadow
                                        offset: Offset(
                                          5.0, // Move to right 10  horizontally
                                          5.0, // Move to bottom 10 Vertically
                                        ),
                                      )
                                    ],
                                    color: Colors.white,
                                    borderRadius: new BorderRadius.only(
                                        topLeft: const Radius.circular(15.0),
                                        topRight: const Radius.circular(15.0),
                                        bottomLeft: const Radius.circular(15.0),
                                        bottomRight:
                                            const Radius.circular(15.0)),
                                  ),
                                  child: Column(
                                    children: <Widget>[
                                      Padding(
                                        padding: EdgeInsets.all(5),
                                      ),
                                      Column(
                                        children: <Widget>[
                                          Container(
                                              margin: EdgeInsets.fromLTRB(
                                                  0, 0, 0, 15),
                                              width: 160,
                                              child: GestureDetector(
                                                onTap: () {
                                                  navigateToSelectTypePage(
                                                      context);
                                                },
                                                child: Image.asset(
                                                  'img/solo.png',
                                                  width: 55,
                                                  height: 50,
                                                ),
                                              )),
                                          Container(
                                              height: 30,
                                              width: 130,
                                              child: RaisedButton(
                                                shape:
                                                    new RoundedRectangleBorder(
                                                        borderRadius:
                                                            new BorderRadius
                                                                .circular(18.0),
                                                        side: BorderSide(
                                                            color: Colors
                                                                .orangeAccent)),
                                                onPressed: () {
                                                  navigateToSelectTypePage(
                                                      context);
                                                },
                                                color: Colors.orangeAccent,
                                                textColor: Colors.white,
                                                child: Text("Nova conta",
                                                    style: TextStyle(
                                                        fontSize: 12)),
                                              )),
                                        ],
                                      )
                                    ],
                                  )),
                              Column(children: list)
                            ]))
                      ])),
                  Padding(
                    padding: EdgeInsets.only(top: 25),
                  ),
                ]),
              ))
        ])));
  }

  Future listMyInstas() async {
    var response = await http.post(Uri.encodeFull(url), headers: {
      "Authorization":
          "Basic ODFkMDQzMGQtNGJlMy00MzBjLTljNTYtZWM2ZWIwNTBmZjZmOjA5MjQyNGMxLTU2ZmItNDk1Yi04YjRiLWMzZWZiNGE5YzFhYg=="
    });
    if (response.statusCode == 200) {
      var jsonResponse = convert.jsonDecode(response.body);
      var getHistory = await http.get(Uri.encodeFull(urlHistory),
          headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
      var history = convert.jsonDecode(getHistory.body);

      var getOffers = await http.get(Uri.encodeFull(urlOffers),
          headers: {"Authorization": "Bearer " + jsonResponse['access_token']});
      offers = convert.jsonDecode(getOffers.body);
      validOffers = 0;

      for (int i = 0; i < history["data"].length; i++) {
        if (history["data"][i]["buyer"]["email"] == user.emailHotmart) {
          verifyDate(
              history["data"][i],
              history["data"][i]["purchase"]["approvedDate"],
              history["data"][i]["offer"]["key"]);
        }
      }
      user.idInstagram = "";
      user.codeHotmart = "";
      user.emailHotmart = "";
      user.emailInstagram = "";
      user.expirationDate = "";
      user.telInstagram = "";
      List<Widget> sublist = new List();
      sublist.clear();
      dbRef
          .reference()
          .child('Users/' + textToMd5(obj.emailHotmart))
          .once()
          .then((DataSnapshot snapshot) {
        if (snapshot != null) {
          var map = new Map<String, dynamic>.from(snapshot.value);
          SystemUser usuario = new SystemUser.fromJson(map);

          if (usuario.instas != null) {
            List<dynamic> instas = usuario.instas.values.toList();
            //Se ainda houver ofertas válidas pode cadastrar uma nova
            // if (usuario.instas.length < validOffers) {

            //}

            for (int i = 0; i < usuario.instas.length; i++) {
              user.type = instas[i]["type"];
              sublist.add(new Column(children: <Widget>[
                Container(
                    height: 150,
                    width: 150,
                    padding: EdgeInsets.all(10),
                    decoration: new BoxDecoration(
                      boxShadow: [
                        BoxShadow(
                          color: Colors.grey,
                          blurRadius: 15.0, // soften the shadow
                          spreadRadius: 1.0, //extend the shadow
                          offset: Offset(
                            5.0, // Move to right 10  horizontally
                            5.0, // Move to bottom 10 Vertically
                          ),
                        )
                      ],
                      color: Colors.white,
                      borderRadius: new BorderRadius.only(
                          topLeft: const Radius.circular(15.0),
                          topRight: const Radius.circular(15.0),
                          bottomLeft: const Radius.circular(15.0),
                          bottomRight: const Radius.circular(15.0)),
                    ),
                    child: Column(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(top: 35),
                        ),
                        Column(
                          children: <Widget>[
                            Container(
                                width: 160,
                                child: GestureDetector(
                                    onTap: () {
                                      _performSelection(instas[i]);
                                    },
                                    child: user.type == "Conta Empresa"
                                        ? new Image.asset(
                                            'img/group.png',
                                            width: 55,
                                            height: 50,
                                          )
                                        : new Image.asset(
                                            'img/solo.png',
                                            width: 55,
                                            height: 50,
                                          ))),
                            Container(
                                margin: EdgeInsets.only(top: 15),
                                height: 30,
                                width: 130,
                                child: RaisedButton(
                                  shape: new RoundedRectangleBorder(
                                      borderRadius:
                                          new BorderRadius.circular(18.0),
                                      side: BorderSide(
                                          color: Colors.orangeAccent)),
                                  onPressed: () {
                                    _performSelection(instas[i]);
                                  },
                                  color: Colors.orangeAccent,
                                  textColor: Colors.white,
                                  child: Text('${instas[i]['type']}',
                                      style: TextStyle(fontSize: 12)),
                                )),
                          ],
                        )
                      ],
                    )),
                Container(
                  margin: EdgeInsets.symmetric(vertical: 10),
                  child: Text(
                    '${instas[i]['idInstagram']}',
                    style: TextStyle(color: Colors.white, fontSize: 18),
                  ),
                ),
                Padding(padding: EdgeInsets.only(top:10),)
              ]));
            }
          }
        }
        this.setState(() {
          list.clear();
          list += sublist;
        });
      });
    }
  }

  verifyDate(order, approvalDate, key) {
    var date = new DateTime.fromMillisecondsSinceEpoch(
            int.parse(approvalDate.toString()))
        .toLocal();
    String signature = "";
    for (int i = 0; i < offers["data"].length; i++) {
      if (offers["data"][i]["key"] == key) {
        signature = offers["data"][i]["planPeriodicity"];
      }
    }

    if (signature == "QUARTERLY") {
      date = date.add(new Duration(days: 90));
    } else if (signature == "BIANNUAL") {
      date = date.add(new Duration(days: 180));
    } else if (signature == "ANNUAL") {
      date = date.add(new Duration(days: 365));
    } else if (signature == "MONTHLY") {
      date = date.add(new Duration(days: 31));
    }
    if (date.isAfter(DateTime.now())) {
      validOffers++;
    }
  }

  void _performSelection(insta) {
    UserData userData = UserData.fromJson(insta);
    user.idInstagram = userData.idInstagram;
    user.codeHotmart = userData.codeHotmart;
    user.emailHotmart = obj.emailHotmart;
    user.emailInstagram = userData.emailInstagram;
    user.expirationDate = "";
    user.telInstagram = userData.telInstagram;
    user.type = userData.type;
    navigateToPersonalPanelPage(context);
  }
}

class RetrieveImage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    if (user.type == "Conta Empresa") {
      return Image.asset(
        'img/group.png',
        width: 55,
        height: 50,
      );
    } else {
      return Image.asset(
        'img/solo.png',
        width: 55,
        height: 50,
      );
    }
  }
}
